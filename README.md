
# ServiceNow API (SNAPI)

This is a java API for making it easier to work with the popular ITSM SaaS offering.

While ServiceNow offers web services for integration, it can be difficult to just make simple calls to send and retrieve data especially if you are dealing with multiple instances (in my case, development, test, training and production).

Refer to the **[SNapi wiki](https://github.com/sdcote/snapi/wiki)** for more information on how to use this in your projects.

All this API uses is the Apache HttpClient library to make connections to a ServiceNow instance and an abstract data type library(DataFrame) to make parsing records much easier.

## Development Status

This library is currently in the prototyping stage and is undergoing constant change and redesign.

No broken builds are checked-in, so what is in this repository should build and provide value. **Do Not Trust This Code Without First Checking It Yourself.**  

Feel free to copy whatever you find useful and please consider contributing so others may benefit as you may have. 

## Project Goals

This project has a simple goal: make Java access to a remote ServiceNow instance as simple as possible.

 * Create an Instance, then interact with ServiceNow tables,
 * Be usable in any ServiceNow deployment,
 * Insert data into any table,
 * Retrieve data from any table,
 * Examine table structure,
 * Discover table structures,
 * Provide utilities to assist in the loading and extraction of data,
 * Provide value first, optimize later.

## Prerequisites:

  * JDK 1.6 or later installed
  * Ability to run bash (*nix) or batch (Windows) scripts
  * Network connection to get the dependencies (there are ways around that)
  * Assumes you do not have gradle installed (if you do, you can replace gradlew with gradle)
