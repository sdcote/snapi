/*
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

import coyote.itsm.connection.ItsmConnection;


/**
 * Represents an Incident record.
 * 
 * <p>Incident records are received from the ITSM system via queries or can be 
 * created by the caller when a new incident needs to be inserted into the ITSM 
 * system or to update an existing ITSM Incident in the system.</p>
 */
public class Incident extends AbstractItsmRecord {
  private static final String TABLENAME = "incident";

  private static final String SHORT_DESC = "short_description";
  
  /** A numeric value of 1(High), 2(Medium), or 3(Low). This is utilized to calculate the Priority of the Ticket.*/
  private static final String URGENCY = "urgency";


  // (Required) Configuration Item: could be Node, Application, Job Name, etc... Must be of a valid CI field format. (e.g. OHLEWAPP0574)
  private String configItem = null;

  // (Required) Catalog ID: Used in lieu of most optional arguments. This ID will identify the event within the ESM Event Catalog and allow for a more static and widely defined source of event attributes. Although customer data can be sent with each event, a CatalogID must still be defined for each type of event. This will be defined by ESM.
  private String CatalogID = null;
  
  // (Required) Key: This is a static numeric identifier given to the customer by ESM. Only defined keys will be processed by Netcool (e.g. C63782E4)
  private String Key = null;
  
  // (Required) Message: The message that will appear in the ticket and to the ECC Operators for this event. This is a 255 character string value.
  private String message = null;
  
  // (Required) Customer Event ID: A unique identifier for the event. Built by the customer and approved by ESM. This value cannot have dynamic information (e.g. date, time, percentages, etc�). This identifier will also have a limited character count; ESM will generally want to keep this to 20 characters or less.
  private String eventID = null;
  
  // (Required) Source: The source of the event, often the Application ID.
  private String source = null;
  
  //Category - A valid Category name for the event type. (e.g. HARDWARE)
  private String Category = null;
  
  // SubCategory - A valid Sub category name for the event type. (e.g. SERVER)
  private String subCategory = null;

  // ProblemType - A valid Problem Type name for the event type. (e.g. HIGH DISK SPACE USAGE)
  private String problemType = null;
  
  // ProductType - A valid Service Center Product Type name for the event type. (e.g. WINDOWS)
  private String productType = null;
  
  // CustomerData - Defined by customer. Can be used to construct more complex messages and can contain dynamic values. This data will be scanned for invalid textual values.
  private String CustomerData = null;
  
  // Group - A valid Assignment Group name, this will be validated before an incident is created. (e.g. NETOPS)
  private String group = null;
  
  // Impact - A numeric value of 1(Large/Critical), 2(Medium/Major), or 3(Small/Minor). This is utilized to calculate the Priority of the Ticket.
  private int impact = 0;
  
  // Urgency - A numeric value of 1(High), 2(Medium), or 3(Low). This is utilized to calculate the Priority of the Ticket.
  private int urgency = 0;


  
  /*
  "impact": "3",
  "correlation_id": "",
  "description": "",
  "priority": "5",
  "sys_updated_on": "2015-04-16 14:24:09",
  "parent": "",
  "number": "INC0010045",
  "closed_by": "",
  "work_start": "",
  "category": "inquiry",
  "incident_state": "1",
  "correlation_display": "",
  "company": "",
  "active": "true",
  "due_date": "",
  "assignment_group": "",
  "caller_id": "1b8998930f9bb100085d6509b1050e50",
  "knowledge": "false",
  "made_sla": "true",
  "parent_incident": "",
  "state": "1",
  "sys_created_on": "2015-04-16 14:24:09",
  "order": "",
  "sys_updated_by": "Netcool1",
  "notify": "1",
  "problem_id": "",
  "sys_id": "7e7f2b480f23f100a5eee478b1050e77",
  "severity": "3",
  "sys_created_by": "Netcool1",
  "assigned_to": "",
  "cmdb_ci": "",
  "opened_by": "1b8998930f9bb100085d6509b1050e50",
  "subcategory": "",
  "sys_class_name": "incident",
  "watch_list": "",
  "time_worked": "",
  "contact_type": "phone",
  "escalation": "0",
  "comments": ""
    */

  public Incident() {}




  public Incident( final ItsmConnection connection ) {
    super( connection, TABLENAME );
  }




  public String getShortDescription() {
    return getAsString( SHORT_DESC );
  }




  public void setShortDescription( final String value ) {
    put( SHORT_DESC, value );
  }




  public void setUrgency( int value ) {
    put( URGENCY, value );
  }




  public int getUrgency() {
    try {
      return getAsInt( URGENCY );
    } catch ( Exception e ) {
      // ignore, possibly not set yet
    }
    return 0;
  }

}
