/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import coyote.itsm.connection.ItsmConnection;
import coyote.itsm.connection.ItsmV1Connection;
import coyote.snow.Parameters;
import coyote.snow.SnowRecord;


/**
 * A basic service management API to assist in abstracting the details of 
 * ServiceNow from an ITSM system.
 * 
 * <p>Some work has been done in the OSS/J project which might be a good source 
 * of models. See: https://www.tmforum.org/ossj/ for more information.</p>
 * 
 * <p>There is also JSR-144 which may be of interest.</p>
 * 
 * <p>The goal of this portion of the API is not to be standards compliant, but 
 * to use ServiceNow via the API in alignment with standard concepts.</p> 
 * 
 * <p>The ServiceManager offers a level of abstraction from the type of service 
 * connection used to connect with ServiceNow. The default connection uses the 
 * JSON REST services.</p> 
 */
public class ServiceManager implements Closeable {

  /** Holds the protocol abstraction this fixture uses to communicate with the ServiceNow instance.*/
  ItsmConnection connection = null;




  /**
   * Create a service management fixture which connects to the given instance.
   * 
   * @param instanceUrl URL to the instance
   * @param username name of the user 
   * @param password credentials for the user
   */
  public ServiceManager( final String instanceUrl, final String username, final String password ) {
    // TODO: lookup system property to see what connection to use...use the default for now

    try {
      connection = new ItsmV1Connection( instanceUrl, username, password );
    } catch ( final IOException e ) {
      throw new IllegalArgumentException( "Connection error: " + e.getMessage(), e );
    }

  }




  public Request buildRequest() {
    return new Request( connection );
  }




  public Request buildRequest( String id ) {
    Request retval = new Request( connection );
    retval.setIdentifier( id );
    return retval;
  }




  public Incident buildIncident() {
    return new Incident( connection );
  }




  public Incident buildIncident( String id ) {
    Incident retval = new Incident( connection );
    retval.setIdentifier( id );
    return retval;
  }




  /**
   * Close the connection to the service management instance.
   * 
   * @throws IOException 
   * 
   * @see java.io.Closeable#close()
   */
  @Override
  public void close() throws IOException {
    if ( connection != null ) {
      connection.close();
    }
  }




  public List<SnowRecord> getRecords( Parameters params ) throws IOException {
    return connection.getRecords( params );
  }




  /**
   * @return the connection used by this service manager.
   */
  public ItsmConnection getConnection() {
    return connection;
  }




  /**
   * Set the ITSM Connection used by this service Manager.
   * 
   * <p>This allows the use of custom connections as opposed to the default 
   * JSON REST connection.</p>
   * 
   * @param connection the connection to set
   */
  public void setConnection( ItsmConnection connection ) {
    this.connection = connection;
  }

}
