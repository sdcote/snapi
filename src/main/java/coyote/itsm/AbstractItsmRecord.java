/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

import java.io.IOException;

import coyote.dataframe.marshal.JSONMarshaler;
import coyote.itsm.connection.ItsmConnection;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;


/**
 * 
 */
public abstract class AbstractItsmRecord extends SnowRecord implements ItsmRecord {
  // abstract data type to hold our data
  protected String tablename = null;
  protected ItsmConnection connection = null;

  public static final String SYS_ID = "sys_id";
  public static final String SYS_UPDATED = "sys_updated_on";
  public static final String SYS_CREATED = "sys_created_on";
  public static final String SHORT_DESC = "short_description";
  public static final String URGENCY = "urgency";




  /**
   * 
   */
  public AbstractItsmRecord() {

  }




  /**
   * 
   * @param connection
   * @param tablename
   */
  public AbstractItsmRecord( final ItsmConnection connection, final String tablename ) {
    setConnection( connection );
    setTablename( tablename );
  }




  public String getIdentifier() {
    return getAsString( SYS_ID );
  }




  public void setIdentifier( String value ) {
    super.put( SYS_ID, new SnowKey( value ).toString() );
  }




  @Override
  public void create() throws ItsmException {
    try {
      connection.create( this );
    } catch ( final IOException e ) {
      throw new ItsmException( "Could not create record", e );
    }
  }




  @Override
  public boolean delete() throws ItsmException {

    try {
      return connection.deleteRecord( getTablename(), new SnowKey( getIdentifier() ) );
    } catch ( IOException e ) {
      throw new ItsmException("Could not delete record", e );
    }
  }




  /**
   * @see coyote.itsm.ItsmRecord#getTablename()
   */
  @Override
  public String getTablename() {
    return tablename;
  }




  @Override
  public void read() throws ItsmException {
    // TODO Auto-generated method stub

  }




  private void setConnection( final ItsmConnection connection ) {
    this.connection = connection;
  }




  private void setTablename( final String tablename ) {
    this.tablename = tablename;
  }




  /**
   * @return Formatted, multi-line JSON string representing the record.
   */
  @Override
  public String toFormattedString() {
    return JSONMarshaler.toFormattedString( this );
  }




  /**
   * JSON formatted string representing the data in the record.
   *  
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return JSONMarshaler.marshal( this );
  }




  @Override
  public void update() throws ItsmException {
    // TODO Auto-generated method stub

  }

}
