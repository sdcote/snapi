/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

/**
 * 
 */
public interface ItsmRecord {

  /**
   * Create the record in the instance
   * @throws ItsmException 
   */
  public void create() throws ItsmException;




  /**
   * Delete the data represented by this record in the instance.
   * 
   * @return true if the delete operation succeeded false otherwise 
   */
  public boolean delete() throws ItsmException;




  public String getTablename();




  /**
   * Read the data representing this record from the instance/
   */
  public void read() throws ItsmException;




  /**
   * Update the instance with the data in this record.
   */
  public void update() throws ItsmException;

}
