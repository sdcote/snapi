/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

import coyote.itsm.connection.ItsmConnection;


/**
 * 
 */
public class Request extends AbstractItsmRecord {

  private static final String TABLENAME = "request";




  protected Request() {}




  public Request( final ItsmConnection connection ) {
    super( connection, TABLENAME );
  }




  public void setShortDescription( final String value ) {
    put( SHORT_DESC, value );
  }




  public String getShortDescription() {
    return getAsString( SHORT_DESC );
  }

}
