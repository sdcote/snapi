/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

/**
 * 
 */
public class ItsmException extends Exception {

  private static final long serialVersionUID = -1947605156309450426L;




  public ItsmException( final String message ) {
    super( message );
  }




  public ItsmException( final String message, final Exception cause ) {
    super( message, cause );
  }
}
