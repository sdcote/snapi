/*
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm.connection;

/**
 * Base class for SM Connections
 */
public abstract class AbstractConnection implements ItsmConnection {

}
