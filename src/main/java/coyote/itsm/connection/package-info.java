/**
 * This package contains an abstraction of the SNapi, hiding some of the API 
 * details for simpler usage.
 */
package coyote.itsm.connection;