/*
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm.connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.dataframe.DataFrame;
import coyote.itsm.ItsmRecord;
import coyote.snow.Instance;
import coyote.snow.Parameters;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;
import coyote.snow.SnowRecordList;


/**
 * 
 */
public class ItsmV1Connection extends AbstractConnection {
  private static final Logger log = LoggerFactory.getLogger( ItsmV1Connection.class );

  Instance instance = null;




  /**
   * Create a connection to the ITSM service.
   * 
   * @param url The URL to the server
   * @param user the user account for the connection.
   * @param pass credentials for the given user account
   * 
   * @throws IOException if the arguments are invalid.
   */
  public ItsmV1Connection( final String url, final String user, final String pass ) throws IOException {
    try {
      instance = new Instance( url, user, pass );
    } catch ( final Exception e ) {
      throw new IOException( e );
    }
  }




  @Override
  public List<SnowRecord> getRecords( Parameters params ) throws IOException {
    List<SnowRecord> retval = new ArrayList<SnowRecord>();

    SnowRecordList results = instance.getTable( params.getTablename() ).getRecords( params );

    for ( SnowRecord record : results ) {
      retval.add( record );
    };

    return retval;
  }




  /**
   * @see java.io.Closeable#close()
   */
  @Override
  public void close() throws IOException {
    if ( instance != null ) {
      instance.close();
    }
  }



  /**
   * @see coyote.itsm.connection.ItsmConnection#deleteRecord(java.lang.String, coyote.snow.SnowKey)
   */
  @Override
  public boolean deleteRecord( String tablename, SnowKey key ) throws IOException {
    return instance.getTable( tablename ).delete( key );
  }




  @Override
  public void create( ItsmRecord record ) throws IOException {
    try {
      // Have the instance create the record  in the table appropriate for the record
      SnowRecord result = instance.getTable( record.getTablename() ).create( (SnowRecord)record );

      // now merge the return value with that of the argument
      ( (DataFrame)record ).merge( (DataFrame)result );

    } catch ( Exception e ) {
      log.warn( "Exception: %s", e.getMessage() );
    }

  }

}
