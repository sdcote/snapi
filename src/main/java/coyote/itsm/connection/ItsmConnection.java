/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm.connection;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import coyote.itsm.ItsmRecord;
import coyote.snow.Parameters;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;


/**
 * Represents a connection to a service management system.
 * 
 */
public interface ItsmConnection extends Closeable {

  /**
   * Delete a record using the given Parameters to specify the record.
   * 
   * <p>All that should be specified is the key (sys_id) of the record to 
   * delete and the table in which it exists.</p>
   * 
   * <p>Successful deletions return without error, exceptions will be thrown 
   * otherwise.</p>
   * 
   * @param tablename name of the table in which the record should be found
   * @param key the primary key / identifier for the record.
   * 
   * @return true if the record was deleted, false if not found
   * 
   * @throws IOException if access control prevented access or other processing error occurred
   */
  public boolean deleteRecord( String tablename, SnowKey key ) throws IOException;




  public void create( ItsmRecord record ) throws IOException;




  List<SnowRecord> getRecords( Parameters params ) throws IOException;





}
