package coyote.snow;

import java.io.IOException;


/**
 * Exception thrown when the user account associated with the connection does 
 * not have the proper access to perform the operation or access the required 
 * table in the SaaS instance.
 */
public class InsufficientRightsException extends IOException {

  private static final long serialVersionUID = 1554556620865880455L;




  public InsufficientRightsException( final SnowTable table, final Exception cause, final String response ) {
    super( response + "\ntable=" + table.getName(), cause );
  }




  public InsufficientRightsException( final SnowTable table, final String message, final String response ) {
    super( response == null ? message + "\ntable=" + table.getName() : message + "\ntable=" + table.getName() + "\n" + response );
  }




  public InsufficientRightsException( final SnowTable table, final String method, final String faultstring, final String response ) {
    this( table, faultstring + "\n" + method + " " + table.getName(), response );
  }

}
