package coyote.snow;

import java.io.IOException;
import java.text.ParseException;

import coyote.dataframe.DataField;
import coyote.dataframe.DataFrame;
import coyote.dataframe.marshal.JSONMarshaler;


/**
 * Represents a single record/row from a table
 */
public class SnowRecord extends DataFrame {

  final protected SnowTable table;
  protected SnowKey key;
  protected SnowDateTime updatedTimestamp;
  protected SnowDateTime createdTimestamp;




  /**
   * Construct a blank record suitable for holding data.
   * 
   * <p>This record is not associated to any table.</p>
   */
  public SnowRecord() {
    table = null;
    key = null;
  }




  public SnowRecord( final SnowKey key ) {
    table = null;
    if ( ( key != null ) && ( key.value != null ) && ( key.value.length() > 0 ) ) {
      this.key = key;
      super.put( ServiceNow.SYS_ID_FIELD, key.toString() );
    } else {
      throw new IllegalArgumentException( "Record key cannot be null or empty" );
    }
  }




  /**
   * called
   * 
   * @param snowTable
   * @param dataframe
   * 
   * @throws SnowException
   */
  protected SnowRecord( final SnowTable snowTable, final DataFrame dataframe ) throws SnowException {
    table = snowTable;

    // Copy all the fields from the given frame
    for ( final DataField field : dataframe.getFields() ) {
      super.fields.add( field );
    }

    final String sysid = getAsString( ServiceNow.SYS_ID_FIELD );

    if ( sysid != null && sysid.length() > 0 ) {
      try {
        key = new SnowKey( sysid );
      } catch ( Exception e ) {
        // Some views will return composite sysids, allow them
        if ( sysid.startsWith( "__ENC__" ) ) {
          key = new SnowKeyComposite( sysid );
        } else {
          throw new SnowException( table, e, "Invalid system identifier: '" + sysid + "'" );
        }
      }
    }

    final String updated_on = getAsString( ServiceNow.SYS_UPDATED_ON_FIELD );
    if ( updated_on != null ) {
      try {
        updatedTimestamp = new SnowDateTime( updated_on );
      } catch ( final ParseException e ) {
        if ( !table.getName().startsWith( "sys_" ) ) {
          throw new SnowException( table, "Invalid sys_updated_on=" + updated_on, toString() );
        }
      }
    }

    final String created_on = getAsString( ServiceNow.SYS_CREATED_ON_FIELD );
    if ( created_on != null ) {
      try {
        createdTimestamp = new SnowDateTime( created_on );
      } catch ( final ParseException e ) {
        if ( !table.getName().startsWith( "sys_" ) ) {
          throw new SnowException( table, "Invalid sys_created_on=" + created_on, toString() );
        }
      }
    }

    super.modified = false;
  }




  /**
   * Get sys_created_on from a SnowRecord object.
   */
  public SnowDateTime getCreatedTimestamp() {
    return createdTimestamp;
  }




  /**
   * Return a DateTime field as a SnowDateTime.
   * 
   * For a Java date use getDateTime(fieldname).toDate().
   */
  public SnowDateTime getDateTime( final String fieldname ) throws ParseException {
    final String value = getFieldValue( fieldname );
    if ( value == null ) {
      return null;
    }

    final SnowDateTime result = new SnowDateTime( value );

    return result;
  }




  /**
   * Return the value of the given field name as a boolean value.
   * 
   * <p><strong>NOTE:</strong> of the field is null or if the value does not 
   * parse into a boolean, the value of {@code false} is returned. An 
   * exception is not thrown for illegal or missing values.</p>
   * 
   * @param fieldname The name of the field to retrieve
   * 
   * @return the value of the field of as a boolean. 
   */
  public boolean getFieldAsBoolean( final String fieldname ) {
    final String value = getFieldValue( fieldname );

    if ( ( value != null ) && ( value.trim().length() > 0 ) ) {
      final String tlc = value.trim().toLowerCase();
      if ( "true".equals( tlc ) || "yes".equals( tlc ) || "1".equals( tlc ) ) {
        return true;
      }
    }

    return false;
  }




  /**
   * Return the value of the given field name as an integer.
   * 
   * <p><strong>NOTE:</strong> of the field is null or if the value does not 
   * parse into a integer, the value of {@code 0} (zero) is returned. An 
   * exception is not thrown for illegal or missing values.</p>
   * 
   * @param fieldname The name of the field to retrieve
   * 
   * @return the value of the field of as an integer. 
   */
  public int getFieldAsInt( final String fieldname ) {
    try {
      return getAsInt( fieldname );
    } catch ( final Exception e ) {
      return 0;
    }
  }




  /**
   * Get the value of a field from a SnowRecord.
   * 
   * If the table has no field by this name then an exception will be thrown.
   * 
   * If the field is empty (zero length) or missing then return null.
   * 
   * @param fieldname Name of SnowRecord field
   * 
   * @return Field value as a string or null if the field is missing or has zero length.
   * 
   * @throws InvalidFieldNameException 
   */
  public String getFieldValue( final String fieldname ) throws InvalidFieldNameException {
    final String result = getAsString( fieldname );
    if ( result == null ) {
      if ( table.isValidated() ) {
        if ( !table.getSchema().isDefined( fieldname ) ) {
          throw new InvalidFieldNameException( fieldname );
        }
      }
      return null;
    }

    if ( result.length() == 0 ) {
      return null;
    }

    return result;
  }




  /**
   * Get the sys_id from a SnowRecord object.  
   * 
   * @return sys_id
   */
  public SnowKey getKey() {
    return key;
  }




  public String getIdentifier() {
    return getAsString( ServiceNow.SYS_ID_FIELD );
  }




  public void setIdentifier( String value ) {
    key = new SnowKey( value );
    super.put( ServiceNow.SYS_ID_FIELD, key.toString() );
  }




  /**
   * If a reference field in a SnowRecord contains a valid sys_id, then
   * return the SnowRecord to which the reference field points.
   * 
   * <p>If the reference field is null then return null.</p>
   * 
   * <p>This function can be used to simulate the "dot walking" behavior
   * of JavaScript.  The following example will set {@code name} to the 
   * manager of the "Network Support" group. The {@code get} function returns 
   * a SnowRecord for the sys_user_group table and the the {@code getRecord} 
   * function returns a SnowRecord  for the sys_user table. However, if the 
   * group has no manager this code will throw a NullPointerException.<pre>
   * String name = instance.table("sys_user_group").get("name", "Network Support").
   *   getRecord("manager").getField("name");
   * </pre></p>
   * 
   * @param fieldname Name of the reference field.
   * 
   * @return A retrieved copy of the record to which the reference field 
   * points, or null if the value of the reference field is null.
   * 
   * @throws IOException
   */
  public SnowRecord getRecord( final String fieldname ) throws IOException {
    final SnowFieldDefinition fielddef = table.getSchema().getFieldDefinition( fieldname );
    if ( fielddef == null ) {
      throw new InvalidFieldNameException( fieldname );
    }
    final String tablename = fielddef.getReference();
    return getRecord( fieldname, table.getInstance().getTable( tablename ) );
  }




  /**
   * If a reference field in a SnowRecord contains a valid sys_id, then 
   * return the SnowRecord to which the reference field points.
   * 
   * <p>If the reference field is null then return null.</p>
   * 
   * <p>This function requires a SnowTable object as the second parameter so 
   * that the function will know which table to query.</p>
   *  
   * @param fieldname Name of the reference field.
   * @param table SnowTable to which the reference field points, or null if 
   * the value of the reference field is null.
   * 
   * @return a record to which the reference field points.
   * 
   * @throws IOException
   */
  public SnowRecord getRecord( final String fieldname, final SnowTable table ) throws IOException {
    final String fieldvalue = getFieldValue( fieldname );
    if ( fieldvalue == null ) {
      return null;
    }
    final SnowKey sysid = new SnowKey( fieldvalue );
    final SnowRecord result = table.get( sysid );
    return result;
  }




  public SnowKey getReference( final String fieldname ) throws InvalidFieldNameException {
    final String value = getAsString( fieldname );

    if ( ( value == null ) || ( value.length() == 0 ) ) {
      return null;
    }
    if ( table.isValidated() ) {
      final SnowFieldDefinition defn = table.getSchema().getFieldDefinition( fieldname );
      if ( defn == null ) {
        throw new InvalidFieldNameException( fieldname );
      }
      if ( !defn.isReference() ) {
        throw new InvalidFieldNameException( fieldname );
      }
    }
    return new SnowKey( value );
  }




  public SnowTable getTable() {
    return table;
  }




  /**
   * Get sys_updated_on from a SnowRecord object.
   */
  public SnowDateTime getUpdatedTimestamp() {
    return updatedTimestamp;
  }




  public int numFields() {
    return super.size();
  }




  /**
   * @return the record as a multi-line, indented JSON String.
   */
  public String toFormattedString() {
    return JSONMarshaler.toFormattedString( this );
  }




  /**
   * @return the record as a JSON formatted string.
   */
  @Override
  public String toString() {
    return JSONMarshaler.marshal( this );
  }

}
