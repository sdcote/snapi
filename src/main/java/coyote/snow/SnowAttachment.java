/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import coyote.commons.ByteUtil;
import coyote.commons.MimeType;
import coyote.commons.StringUtil;


/**
 * Model of an attachment in the system.
 */
public class SnowAttachment {
  private File file = null;
  private String tableName = null;
  private String recordId = null;
  private String attachmentId = null; // from the Attachment table




  /**
   * Create an attachment from a file reference
   * 
   * @param file the file to attach
   * 
   * @throws IllegalArgumentException if the file is null, does not exist or cannot be read.
   */
  public SnowAttachment( File file ) {
    if ( file != null ) {
      if ( file.exists() ) {
        if ( file.canRead() ) {
          this.file = file;

        } else {
          throw new IllegalArgumentException( "Cannot read attachment file: permissions/access error" );
        }
      } else {
        throw new IllegalArgumentException( "Attachment file does not exist: '" + file.getAbsolutePath() + "'" );
      }
    } else {
      throw new IllegalArgumentException( "Null file reference passed to SnowAttachment constructor" );
    }
  }




  /**
   * @return the name of the table to which this attachment is to be applied 
   */
  public String getTableName() {
    return tableName;
  }




  /**
   * @param tableName the name of the table to which this attachment is to be applied.
   */
  public void setTableName( String tableName ) {
    this.tableName = tableName;
  }




  /**
   * @return the the {@code sys_id} of the record to which this attachment is to be applied
   */
  public String getRecordId() {
    return recordId;
  }




  /**
   * @param sys_id the record identifier to which this attachment is to be applied
   */
  public void setRecordId( String sys_id ) {
    recordId = sys_id;
  }




  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer b = new StringBuffer( "SnowAttachment: " );
    b.append( this.getName() );
    b.append( " : " );
    b.append( this.getMimeType() );
    if ( StringUtil.isNotBlank( attachmentId ) ) {
      b.append( "ID:" );
      b.append( attachmentId );
      b.append( " - Attached to " );
      b.append( tableName );
      b.append( ":" );
      b.append( recordId );
    }
    return b.toString();
  }




  /**
   * Read the entire file into memory as an array of bytes.
   *
   * @return A byte array that contains the contents of the file.
   *
   * @throws IOException If problems occur.
   */
  public byte[] getContents() throws IOException {
    if ( file == null ) {
      throw new IOException( "File reference was null" );
    }

    if ( file.exists() && file.canRead() ) {
      DataInputStream dis = null;
      final byte[] bytes = new byte[new Long( file.length() ).intValue()];

      try {
        dis = new DataInputStream( new FileInputStream( file ) );

        dis.readFully( bytes );

        return bytes;
      } catch ( final Exception ignore ) {}
      finally {
        // Attempt to close the data input stream
        try {
          if ( dis != null ) {
            dis.close();
          }
        } catch ( final Exception ignore ) {}
      }
    }

    return null;
  }




  /**
   * Read and encode entire file into base64 encoding.
   *
   * @return A base64 string representing the encoded contents of the file.
   *
   * @throws IOException If problems occur.
   */
  public String getEncodedContents() throws IOException {
    byte[] data = getContents();
    String base64 =  ByteUtil.toBase64( data );
    return base64;
  }




  /**
   * @return the name of this attachment excluding the path
   */
  public String getName() {
    if ( file != null ) {
      return file.getName();
    }
    return null;
  }




  /**
   * @return the MIME type for this file.
   */
  public String getMimeType() {
    return MimeType.get( this.getName() ).get( 0 ).getType();
  }




  /**
   * @return the system identifier of the attachment
   */
  public String getAttachmentId() {
    return attachmentId;
  }




  /**
   * @param sysId the Attachment Id to set
   */
  void setAttachmentId( String sysId ) {
    attachmentId = sysId;
  }




  /**
   * Set the attachment identifier...the system id for this attachment.
   * 
   * @param key the sys_id for this attachment in the system
   */
  void setAttachmentId( SnowKey key ) {
    if ( key != null ) {
      this.setAttachmentId( key.value );
    }

  }

}
