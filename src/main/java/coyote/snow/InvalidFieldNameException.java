package coyote.snow;

/**
 * Exception thrown when the requested field does not exist in the table 
 * associated with the current operation.
 */
public class InvalidFieldNameException extends IllegalArgumentException {

  private static final long serialVersionUID = 2988776478441604116L;




  public InvalidFieldNameException( final String name ) {
    super( "InvalidFieldName: " + name );
  }

}
