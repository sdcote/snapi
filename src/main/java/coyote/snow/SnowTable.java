package coyote.snow;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.dataframe.DataField;
import coyote.dataframe.DataFrame;
import coyote.snow.worker.Response;


/**
 * Used to access a ServiceNow table.
 * 
 * <p>The recommended way to obtain a SnowTable object is the 
 * {@code Instance.getTable(String tablename)} method.</p>
 * 
 * <p>This class will load the table schema from sys_dictionary if the instance 
 * has had its schemas loaded, otherwise the schemas will be left to null.</p>
 */
public class SnowTable {
  final String tablename;
  final String parentname;
  protected String type = "table";
  private final Instance instance;
  private boolean validate = true;
  private SnowTableSchema schema = null;
  Logger log = LoggerFactory.getLogger( getClass() );




  /**
   * The recommended way to obtain a SnowTable object is the 
   * {@code Instance.getTable(String)} method.
   * 
   * @param tablename
   * @param instance
   * 
   * @throws IOException 
   */
  public SnowTable( final Instance instance, final String tablename ) throws IOException, InvalidTableNameException {
    this.tablename = tablename;
    this.instance = instance;
    validate = instance.validate;
    parentname = determineParentName();
    if ( instance.isUsingSchemas ) {
      loadSchema();
    }
  }




  /**
   * Create the record in the given table and return the result.
   * 
   * <p>When inserting into a table, the complete record is usually returned 
   * with all the fields in that record being returned. When creating a record 
   * in an import set, it is possible for more than one record to be returned 
   * depending on what the transform map is configured to do. In such cases, 
   * this method will only return the first record returned.</p>
   * 
   * @param record What data is to be inserted
   * 
   * @return the results of the operation (usually the entire table record)
   * 
   * @throws IOException if communications errors have occurred
   */
  public SnowRecord create( final SnowRecord record ) throws IOException {
    final DataFrame responseFrame = instance.getWorker().postRecord( this, record );

    if ( responseFrame != null ) {

      // Sometimes creating a record in an import set results in multiple 
      // records being returned, just return the first one (for now)
      if ( responseFrame.isArray() ) {
        for ( int indx = 0; indx < responseFrame.size(); indx++ ) {
          final DataField field = responseFrame.getField( indx );
          if ( field.isFrame() ) {
            return new SnowRecord( this, (DataFrame)field.getObjectValue() );
          }
        }
      } else {
        return new SnowRecord( this, responseFrame );
      }
    }

    return null;
  }




  /**
   * Delete the given record from this tables representation in the instance.
   * 
   * @return true if the deletion succeeded, false otherwise
   * 
   * @param key the key (sys_id) of the record to delete.
   * 
   * @throws IOException
   */
  public boolean delete( final SnowKey key ) {
    if ( key != null ) {
      try {
        return instance.getWorker().deleteRecord( this, key );
      } catch ( final IOException e ) {
        log.error( e.getMessage() );
      }
    }
    return false;
  }




  private String determineParentName() throws IOException {
    if ( tablename.equals( ServiceNow.SYS_DB_OBJ ) || tablename.equals( ServiceNow.SYS_DICTIONARY ) ) {
      return null;
    }

    // Only perform parent checks if we are using schemas
    if ( instance.isUsingSchemas ) {
      // Get the sys_db_object table which holds the table hierarchy
      final SnowTable hierarchy = instance.getTables();

      if ( hierarchy != null ) {
        // get the record for this table by its name
        SnowRecord h = null;
        try {
          h = hierarchy.get( "name", tablename );
        } catch ( Exception e ) {
          // it is entirely possible that this is a view and not a table and 
          // therefore will not have a parent table.
          log.info( "Could not determine parent table for %s reason: %s", tablename, e.getMessage() );
        }
        if ( h == null ) {
          return null;
        }

        final SnowRecord result = h.getRecord( "super_class", hierarchy );
        if ( result == null ) {
          return null;
        }
        return result.getAsString( "name" );
      } else {
        return null;
      }
    } else {
      // we are not using schemas, so there are no parents for any table
      return null;
    }
  }




  /**
   * Returns a single record based on a sys_id.
   * 
   * <p>If no qualifying record is found then null is returned.
   * <code>
   * SnowRecord record = instance.getTable("sys_user_group").get(new SnowKey("e8e875b0c0a80164009dc852b4d677d5"));
   * </code>
   * </p>
   * 
   * @param sysid
   * 
   * @return Retrieved copy of the SnowRecord
   * 
   * @throws IOException
   */
  public SnowRecord get( final SnowKey sysid ) throws IOException {

    final String value = sysid.toString();
    if ( ( value == null ) || ( value.length() == 0 ) ) {
      throw new IllegalArgumentException( "missing sys_id" );
    }

    final Parameters params = new Parameters();
    params.setKey( new SnowKey( value ) );
    params.setTablename( tablename );

    final DataFrame responseFrame = instance.getWorker().getRecord( params );

    return responseFrame != null ? new SnowRecord( this, responseFrame ) : null;

  }




  /**
   * Retrieves a single record.
   * 
   * <p>This function should be used in cases where the field value is known 
   * to be unique. If no qualifying records are found this function will 
   * return null. If multiple qualifying records are found this function will 
   * throw an IndexOutOfBoundsException.<pre>
   * SnowRecord rec = instance.getTable("sys_user_group").get("name", "Network Support");
   * </pre></p>
   * 
   * @param fieldname Field name, e.g. "number" or "name"
   * @param fieldvalue Field value
   * 
   * @return A SnowRecord object.
   * 
   * @throws IOException
   * @throws IndexOutOfBoundsException Thrown if more than one record matches the fieldvalue.
   */
  private SnowRecord get( final String fieldname, final String fieldvalue ) throws IOException, IndexOutOfBoundsException {

    //setup the parameters for our request including the filter
    final Parameters params = new Parameters( tablename, new SnowFilter( fieldname, Predicate.IS, fieldvalue ) );

    final SnowRecordList result = getRecords( params );
    final int size = result.size();
    if ( size == 0 ) {
      return null;
    }
    if ( size > 1 ) {
      throw new IndexOutOfBoundsException( String.format( "get %s=%s returned %d records", fieldname, fieldvalue, size ) );
    }
    return result.get( 0 );
  }




  /**
   * @return the reference to the instance hosting this service
   */
  public Instance getInstance() {
    return instance;
  }




  /**
   * @return the name of this table
   */
  public String getName() {
    return tablename;
  }




  /**
   * @return the name of this table's parent, or null if this table has no parent
   */
  public String getParentName() {
    return parentname;
  }




  /**
   * This makes a service call to the instance to retrieve multiple records 
   * using the parameters provided.
   * 
   * @param params The request parameters including the table name and any query
   * 
   * @return the list of records matching the query parameters
   * 
   * @throws IOException
   */
  public SnowRecordList getRecords( final Parameters params ) throws IOException {

    if ( params != null ) {
      params.setTablename( tablename );

      // Get the response data, in it contains error and other information
      final Response response = instance.getWorker().getRecords( params );

      // Get a list of Abstract Data Type objects from the worker
      // representing the records
      final List<DataFrame> records = response.getResults();

      // display some performance metrics in the debug logs
      if ( log.isDebugEnabled() ) {
        log.debug( "HTTP Response time %s", response.getResponseTime() );
        if ( response.getServerElapsed() > 0 ) {
          log.debug( "Reported server time %s", response.getServerTime() );
        }
        log.debug( "Parsing time: %s", response.getParsingTime() );
        log.debug( "Parsing time per record: %s ms", response.getParsingTimePerRecord() );
        log.debug( "Transaction time: %s", response.getTransactionTime() );
        log.debug( "Records per second: %s", response.getRecordsPerSecond() );
      }

      final SnowRecordList list = new SnowRecordList( records.size() );
      for ( final DataFrame record : records ) {
        final SnowRecord rec = new SnowRecord( this, record );
        list.add( rec );
      }

      return list;
    } else {

      // no parameters = no records returned
      return new SnowRecordList();

    }
  }




  public SnowTableSchema getSchema() {
    return schema;
  }




  /**
   * @return the type of table this is; either "table" or "import"
   */
  public String getType() {
    return type;
  }




  public boolean isValidated() {
    return validate;
  }




  /**
   * Load the table schema from sys_dictionary and sys_db_object.
   */
  protected SnowTable loadSchema() throws IOException {
    // create a new schema for this table
    schema = new SnowTableSchema( this );
    return this;
  }




  public void setValidate( final boolean flag ) {
    validate = flag;
  }




  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return instance.getURI().toString() + " - " + tablename;
  }




  /**
   * Update the record in this table with the given record.
   * 
   * <p>The record MUST contain the {@code sys_id} field populated with the 
   * system identifier of the record to update.</p>
   * 
   * @param record
   * 
   * @return The complete version of the record updated 
   * 
   * @throws IOException 
   */
  public SnowRecord update( final SnowRecord record ) throws IOException {
    final DataFrame responseFrame = instance.getWorker().putRecord( this, record );

    return responseFrame != null ? new SnowRecord( this, responseFrame ) : null;
  }




  /**
   * This makes a service call to the instance to retrieve multiple records 
   * using the provided filter and default parameters.
   * 
   * @param filter The query filter to use in selecting records
   * 
   * @return the list of records matching the query
   * 
   * @throws IOException if problems occur during processing
   */
  public SnowRecordList getRecords( SnowFilter filter ) throws IOException {
    return getRecords( new Parameters( tablename, filter ) );
  }

}
