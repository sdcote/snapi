package coyote.snow;

import java.util.ArrayList;


/**
 * An array of SnowRecords.
 */
public class SnowRecordList extends ArrayList<SnowRecord> {

  private static final long serialVersionUID = 1L;




  SnowRecordList() {
    super();
  }




  SnowRecordList( final int size ) {
    super.ensureCapacity( size );
  }




  @Override
  public SnowRecordIterator iterator() {
    return new SnowRecordIterator( this );
  }
}
