package coyote.snow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


/**
 * This class holds a list of request parameters.
 * 
 * <p>When requests are made of the instance, this object is used to determine 
 * what parameters are to be applied to the request. It contains not only the 
 * filter, but other request parameters which affect how the request is 
 * processed and the results returned.</p>
 * 
 * <p>This data type also holds the name of the table against which the query 
 * is executed.</p>
 */
public class Parameters {

  // several static constants
  public static final String QUERY = "sysparm_query";
  public static final String DISPLAY_VALUE = "sysparm_display_value";
  public static final String EXCLUDE_REFERENCE_LINK = "sysparm_exclude_reference_link";
  public static final String FIELDS = "sysparm_fields";
  public static final String LIMIT = "sysparm_limit";
  public static final String VIEW = "sysparm_view";
  public static final String OFFSET = "sysparm_offset";
  private static final String FALSE = "false";
  private static final String ALL = "all";
  private static final String TRUE = "true";

  // display the values of referenced fields can be false(default), true or all
  private String displayValues = FALSE;

  // The name of the table to query
  private String tablename = null;

  // The unique key of the record to return
  private SnowKey key = null;

  // The filter used to query records
  private SnowFilter filter = null;

  // by default, we exclude reference links
  private boolean exclude_reference_links = true;

  // The maximum number of records to return in any request 
  private long limit = 0;

  // The list of fields to show
  private List<String> fields = new ArrayList<String>();

  // request the records at an offset to the beginning
  private long offset = -1;

  // Used by the SOAP worker to represent the SOAP operation to call
  private String operation = null;

  // The view used to control the fields returned
  private String view = null;

  // The purpose of this attribute is to support the use of a specifically crafted filter query for SOAP calls
  private String rawFilter = null;




  public Parameters() {
    super();
  }




  public Parameters( final Parameters base ) {
    super();
    filter = base.filter;
    tablename = base.tablename;
  }




  public Parameters( final String tablename ) {
    super();
    this.tablename = tablename;
  }




  public Parameters( final SnowFilter filter ) {
    super();
    this.filter = filter;
  }




  public Parameters( final String tablename, final SnowFilter filter ) {
    super();
    this.filter = filter;
    this.tablename = tablename;
  }




  public Parameters( final String tablename, final SnowKey key ) {
    super();
    this.key = key;
    this.tablename = tablename;
  }




  /**
   * @return true if it is desired to exclude reference links and just return system identifiers
   */
  public boolean excludeReferenceLinks() {
    return exclude_reference_links;
  }




  /**
   * @return true to return display values for all of the fields, false returns actual values from the database, all returns both or null to use the default setting, 
   */
  public String getDisplayValues() {
    return displayValues;
  }




  /**
   * @return the filter
   */
  public SnowFilter getFilter() {
    return filter;
  }




  /**
   * @return the value of the parameter representing 'sys_id'
   */
  public SnowKey getKey() {
    return key;
  }




  /**
   * @return the currently set limit, 0 = not specified
   */
  public long getLimit() {
    return limit;
  }




  /**
   * @return the value of the parameter named 'sys_id'
   */
  public String getSysId() {
    return key.toString();
  }




  /**
   * @return the name of the source table to which these query parameters 
   * apply or null if the table name has not been set.
   */
  public String getTablename() {
    return tablename;
  }




  /**
   * Set the data retrieval operation for reference and choice fields.
   * 
   * <p>Set this parameter to one of these values:<ul>
   * <li>true - returns display values for all of the fields.</li>
   * <li>false - returns actual values from the database.</li></ul></p>
   * 
   * <p>This parameter is often needed to display comments and work notes as 
   * these are stored in other tables.</p>
   * 
   * <p><strong>Note:</strong> Retrieving display values may cause performance 
   * issues since the request must access fields and records on additional 
   * tables.</p>
   * 
   * @param flag true to retrieve display values, false to just return the record.
   * 
   * @see #setDisplayValuesAll()
   */
  public void setDisplayValues( final boolean flag ) {
    if ( flag ) {
      displayValues = TRUE;
    } else {
      displayValues = FALSE;
    }
  }




  /**
   * Set the data retrieval operation for reference and choice fields to return 
   * both actual and display values.
   * 
   * <p>Note: Retrieving display values may cause performance issues since the 
   * request must access fields and records on additional tables.</p>
   * 
   * @see #setDisplayValues(boolean)
   */
  public void setDisplayValuesAll() {
    displayValues = ALL;
  }




  /**
   * @param flag true to exclude reference links and just return system identifiers false will return a complex value of a url link and a system identifier
   */
  public void setExcludeReferenceLinks( final boolean flag ) {
    exclude_reference_links = flag;
  }




  /**
   * @param filter the filter to set
   */
  public void setFilter( final SnowFilter filter ) {
    this.filter = filter;
  }




  public void setKey( final SnowKey value ) {
    key = value;
  }




  /**
   * Limit the number of responses returned from the result.
   * 
   * <p>A common value for testing is 10. The default value of 0 will result in 
   * no limit being specified in the request and is left to the service to 
   * determine the limit of the results.</p>
   * 
   * <p><strong>NOTE:</strong> setting the limit too high can impair 
   * performance if the query is too broad.
   * 
   * @param limit the number of result records to retrieve,  0 = no limit specified
   */
  public void setLimit( final long limit ) {
    this.limit = limit;
  }




  /**
   * Set the name of the source table to which these query parameters apply.
   * 
   * @param tablename the name of the source table to which these query 
   * parameters apply
   */
  public void setTablename( final String tablename ) {
    this.tablename = tablename;
  }




  /**
   * @return true if the parameters indicate display values should be returned, false to return system identifiers.
   */
  public boolean isDisplayingValues() {
    return ( !FALSE.equalsIgnoreCase( displayValues ) );
  }




  /**
   * Add the given field name to the list of fields to return
   * 
   * @param name the name of the field to add to the list of fields to return
   */
  public void addField( String name ) {
    if ( name != null && !fields.contains( name ) ) {
      fields.add( name.toLowerCase() );
    }
  }




  /**
   * Remove the given name from the list of fields to return.
   * 
   * @param name the name of the field to remove from the list
   */
  public void removeField( String name ) {
    if ( name != null ) {
      fields.remove( name );
    }
  }




  /** 
   * Access the number of fields in the list of fields to return.
   * 
   * <p>If greater than zero, it should be assumed the parameters indicate that 
   * only the fields in the list should be returned in any request.</p>
   * 
   * @return the number of fields set in the field list, zero indicates all fields normally sent as the result.
   */
  public int getFieldListSize() {
    return fields.size();
  }




  /**
   * @return the collection of field names to return with a request.
   */
  public Collection<String> getFields() {
    return fields;
  }




  /**
   * @return the iterator over the names of the fields to return in the request
   */
  public Iterator<String> getFieldIterator() {
    return getFields().iterator();
  }




  /**
   * @return the list of fields to include in the request as a string
   * 
   * @see #getFieldListSize()
   */
  public String getFieldList() {
    StringBuffer b = new StringBuffer();

    final Iterator<String> iter = getFieldIterator();
    while ( iter.hasNext() ) {
      b.append( iter.next() );
      if ( iter.hasNext() ) {
        b.append( "," );
      }
    }

    return b.toString();
  }




  /**
   * @return the offset from the beginning of the result set.
   */
  public long getOffset() {
    return offset;
  }




  /**
   * Allow the user to request an offset from the beginning of the result set 
   * to support pagination.
   * 
   * <p>This is usually used with the LIMIT parameter in something like the 
   * following: {@code https://myinstance.service-now.com/api/now/table/incident?sysparm_limit=10&sysparm_offset=10}</p>

   * @param offset the offset to set
   */
  public void setOffset( long offset ) {
    this.offset = offset;
  }




  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer b = new StringBuffer( "Parameters: table:" );
    b.append( getTablename() );
    b.append( " limit:" );
    b.append( getLimit() );
    b.append( " offset:" );
    b.append( getOffset() );
    b.append( " DisplayValues:" );
    b.append( this.getDisplayValues() );
    b.append( " ExcludeLinks:" );
    b.append( excludeReferenceLinks() );
    b.append( " key:" );
    b.append( getKey() );
    b.append( " filter:" );
    b.append( getFilter() );
    b.append( " fields:" );
    b.append( getFields() );
    b.append( " view:" );
    b.append( getView() );
    b.append( " operation:" );
    b.append( getOperation() );

    return b.toString();
  }




  /**
   * @return the SOAP operation to perform
   */
  public String getOperation() {
    return operation;
  }




  /**
   * Set the web service operation to perform
   * @param operation the SOAP operation to perform
   */
  public void setOperation( String operation ) {
    this.operation = operation;
  }




  /**
   * Set the view which controls the values returned.
   * @param value
   */
  public void setView( String value ) {
    view = value;
  }




  /**
   * @return the name of the view which controls the values returned.
   */
  public String getView() {
    return view;
  }




  /**
   * @return the raw filter text to use in SOAP queries.
   */
  public String getRawFilter() {
    return rawFilter;
  }




  /**
   * Statically set the filter text to send in the SOAP query.
   * 
   * <p>There are times when ServiceNow just can't parse a query when sent from 
   * a SOAP client and special encoding instructions are needed. The parsing of 
   * SOAP queries seems to change with the upgrades of ServiceNow, so this 
   * method allows for setting the literal value of the query.</p>
   * 
   * <p>This is particularly useful if you want to set the filter to use the 
   * javascript functions such as {@code @javascript:gs.dateGenerate(bla)}.</p>
   * 
   * @param text the raw filter to use
   */
  public void setRawFilter( String text ) {
    this.rawFilter = text;
  }

}
