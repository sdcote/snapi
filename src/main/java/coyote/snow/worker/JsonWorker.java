/*
 *
 */
package coyote.snow.worker;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.commons.ByteUtil;
import coyote.commons.StringUtil;
import coyote.dataframe.DataFrame;
import coyote.dataframe.marshal.JSONMarshaler;
import coyote.snow.Instance;
import coyote.snow.InvocationException;
import coyote.snow.Parameters;
import coyote.snow.ServiceNow;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;
import coyote.snow.SnowService;
import coyote.snow.SnowTable;


/**
 * This worker uses JSON to interact with the ServiceNow instance.
 * 
 * This uses the Table API for data retrieval:
 * http://wiki.servicenow.com/index.php?title=Table_API
 */
public class JsonWorker extends AbstractWorker implements InstanceWorker {

  /** The logger this class uses */
  private static final Logger log = LoggerFactory.getLogger( JsonWorker.class );




  public JsonWorker( final Instance instance ) {
    super( instance );

  }




  /**
   * @see coyote.snow.worker.AbstractWorker#close()
   */
  @Override
  public void close() throws IOException {
    log.debug( "Closing Worker..." );
    super.close();
    try {
      httpClient.close();
    } catch ( final IOException e ) {
      log.warn( e.getMessage() );
    }
    finally {
      log.debug( "...Worker closed" );
    }
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#deleteRecord(coyote.snow.SnowTable, coyote.snow.SnowKey)
   */
  @Override
  public boolean deleteRecord( final SnowTable table, final SnowKey key ) throws IOException {

    if ( ( key != null ) && ( table != null ) ) {
      log.debug( "deleting record for: " + key.toString() );

      final StringBuffer b = new StringBuffer( instance.getURI().toString() );
      if ( b.charAt( b.length() - 1 ) != '/' ) {
        b.append( '/' );
      }
      b.append( "api/now/table/" );
      b.append( table.getName() );
      b.append( "/" );
      b.append( key.toString() );

      // Create the request for the URI
      final HttpDelete request = new HttpDelete( b.toString() );
      request.setConfig( config );

      // Set the content type to JSON
      request.setHeader( "content-type", "application/json" );
      request.setHeader( "accept", "application/json" );

      // execute the request
      final Response response = execute( request );

      // throw an exception if we received an error frame
      if ( response.getErrorFrame() != null ) {
        throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
      }

      // The HTTP status code determines if everything worked as expected
      if ( ( response.getHttpStatusCode() >= 200 ) && ( response.getHttpStatusCode() < 300 ) ) {
        return true;
      }
    }

    return false;
  }




  /**
   * Get a single record by its system identifier (sys_id)
   * 
   * GET https://myinstance.service-now.com/api/now/table/{tableName}/{sys_id}
   * 
   * sys_id = the system identifier of the record to retrieve REQUIRED!
   * 
   * @param params
   * 
   * @return the record matching the given parameters or a record containing error information
   * 
   * @throws IOException 
   */
  @Override
  public DataFrame getRecord( final Parameters params ) throws IOException {
    DataFrame retval = null;

    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }
    b.append( "api/now/table/" );

    if ( params.getTablename() != null ) {
      b.append( params.getTablename() );
    } else {
      return retval;
    }

    if ( params.getSysId() != null ) {
      b.append( "/" );
      b.append( params.getSysId() );
    } else {
      return retval;
    }

    // Support specifying a list of fields to return in the response
    if ( params.getFieldListSize() > 0 ) {
      if ( b.indexOf( "?" ) > -1 ) {
        b.append( "&" );
      } else {
        b.append( "?" );
      }
      b.append( Parameters.FIELDS );
      b.append( "=" );
      b.append( params.getFieldList() );
    }

    // Support the Display Values parameter
    if ( params.isDisplayingValues() ) {
      if ( b.indexOf( "?" ) > -1 ) {
        b.append( "&" );
      } else {
        b.append( "?" );
      }
      b.append( Parameters.DISPLAY_VALUE );
      b.append( "=" );
      b.append( params.getDisplayValues() );
    }

    // Create the request for the URI
    final HttpGet request = new HttpGet( b.toString() );
    request.setConfig( config );

    // Set the content type to JSON
    request.setHeader( "content-type", "application/json" );
    request.setHeader( "accept", "application/json" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // get the results of processing the request, should only be 1
    retval = response.getFrame( 0 );

    if ( retval != null ) {
      log.debug( "There are %s fields in the returned record.", retval.size() );
    } else {
      log.error( "No response record received from GET call to %s table", params.getTablename() );
    }
    return retval;
  }




  /**
   * Format:
   * GET https://myinstance.service-now.com/{serviceName}
   * 
   * @throws IOException 
   * 
   * @see coyote.snow.worker.InstanceWorker#getRecords(coyote.snow.Parameters)
   */
  @Override
  public Response invoke( SnowService snowService, Parameters params ) throws IOException {

    log.debug( "Service Request %s", params );
    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }
    if ( snowService.getName() != null ) {
      b.append( snowService.getName() );
    } else {
      return null;
    }

    log.debug( "Executing service call %s", b.toString() );

    // Create the request for the URI
    final HttpGet request = new HttpGet( b.toString() );
    request.setConfig( config );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Try forced preemptive authentication to avoid HTTP redirect to SAML SSO
    b.delete( 0, b.length() ); // clear out the buffer

    if ( StringUtil.isNotBlank( instance.getUser() ) ) {
      b.append( instance.getUser() );
    }
    b.append( ":" );
    if ( StringUtil.isNotBlank( instance.getPassword() ) ) {
      b.append( instance.getPassword() );
    }
    request.setHeader( "Authorization", "Basic " + ByteUtil.toBase64( StringUtil.getBytes( b.toString() ) ) );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Set the content type to JSON
    request.setHeader( "content-type", "application/json" );
    request.setHeader( "accept", "application/json" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // return the results of processing the request
    return response;
  }




  /**
   * Format:
   * GET https://myinstance.service-now.com/api/now/table/{tableName}
   * 
   * Example:
   * https://myinstance.service-now.com/api/now/table/sys_db_object?sysparm_limit=10
   * 
   * Try the REST API explorer:
   * https://myinstance.service-now.com/$restapi.do
   * 
   * @throws IOException 
   * 
   * @see coyote.snow.worker.InstanceWorker#getRecords(coyote.snow.Parameters)
   */
  @Override
  public Response getRecords( final Parameters params ) throws IOException {
    log.debug( "Request %s", params );
    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }
    b.append( "api/now/table/" );

    if ( params.getTablename() != null ) {
      b.append( params.getTablename() );
    } else {
      return null;
    }

    // if we have a key, this is a request for a specific record
    if ( params.getKey() != null ) {
      b.append( '/' );
      b.append( params.getKey().toString() );
    }

    // If we don't want the extra link information, reflect that on the
    // request
    if ( params.excludeReferenceLinks() ) {
      if ( b.indexOf( "?" ) > -1 ) {
        b.append( "&" );
      } else {
        b.append( "?" );
      }
      b.append( Parameters.EXCLUDE_REFERENCE_LINK );
      b.append( "=true" );
    }

    // Support specifying a list of fields to return in the response
    if ( params.getFieldListSize() > 0 ) {
      if ( b.indexOf( "?" ) > -1 ) {
        b.append( "&" );
      } else {
        b.append( "?" );
      }
      b.append( Parameters.FIELDS );
      b.append( "=" );
      b.append( params.getFieldList() );
    }

    // Support the Display Values parameter
    if ( params.isDisplayingValues() ) {
      if ( b.indexOf( "?" ) > -1 ) {
        b.append( "&" );
      } else {
        b.append( "?" );
      }
      b.append( Parameters.DISPLAY_VALUE );
      b.append( "=" );
      b.append( params.getDisplayValues() );
    }

    // if there is no key, this is a query for multiple records, apply other
    // parameters as appropriate
    if ( params.getKey() == null ) {

      // Support the limit parameter
      if ( params.getLimit() > 0 ) {
        if ( b.indexOf( "?" ) > -1 ) {
          b.append( "&" );
        } else {
          b.append( "?" );
        }
        b.append( Parameters.LIMIT );
        b.append( "=" );
        b.append( params.getLimit() );
      }

      // Support specifying an offset in the result set to perform pagination
      if ( params.getOffset() >= 0 ) {
        if ( b.indexOf( "?" ) > -1 ) {
          b.append( "&" );
        } else {
          b.append( "?" );
        }
        b.append( Parameters.OFFSET );
        b.append( "=" );
        b.append( params.getOffset() );
      }

      // Add query parameter if it exists
      if ( params.getFilter() != null ) {
        if ( b.indexOf( "?" ) > -1 ) {
          b.append( "&" );
        } else {
          b.append( "?" );
        }

        // include the filter query
        b.append( Parameters.QUERY );
        b.append( "=" );
        b.append( params.getFilter().toEncodedString() );
      }
      
      // Support specifying an offset in the result set to perform pagination
      if ( params.getView() != null ) {
        if ( b.indexOf( "?" ) > -1 ) {
          b.append( "&" );
        } else {
          b.append( "?" );
        }
        b.append( Parameters.VIEW );
        b.append( "=" );
        b.append( params.getView() );
      }
    }

    log.debug( "Executing service call %s", b.toString() );

    // Create the request for the URI
    final HttpGet request = new HttpGet( b.toString() );
    request.setConfig( config );

    // Set the content type to JSON
    request.setHeader( "content-type", "application/json" );
    request.setHeader( "accept", "application/json" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // return the results of processing the request
    return response;
  }




  /**
   * POST creates a record.
   * POST https://myinstance.service-now.com/api/now/table/{tableName}
   * 
   * @throws IOException 
   * 
   * @see coyote.snow.worker.InstanceWorker#postRecord(coyote.snow.SnowTable, coyote.snow.SnowRecord)
   */
  @Override
  public DataFrame postRecord( final SnowTable snowTable, final SnowRecord record ) throws IOException {
    DataFrame retval = null;

    log.debug( "Posting record for %s %s", snowTable.getName(), snowTable.getType() );

    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }
    b.append( "api/now/" );
    b.append( snowTable.getType() );
    b.append( "/" );

    if ( snowTable.getName() != null ) {
      b.append( snowTable.getName() );
    } else {
      return retval;
    }

    // If we don't want the extra link information, reflect that on the
    // request
    if ( instance.getDefaultParameters().excludeReferenceLinks() ) {
      b.append( "?" );
      b.append( Parameters.EXCLUDE_REFERENCE_LINK );
      b.append( "=true&" );
    }

    // Create the request for the URI
    final HttpPost request = new HttpPost( b.toString() );
    request.setConfig( config );

    try {
      log.debug( "POST DATA:\r\n%s", record.toString() );

      final HttpEntity entity = new ByteArrayEntity( JSONMarshaler.marshal( record ).getBytes( "utf-8" ) );
      request.setEntity( entity );
    } catch ( final UnsupportedEncodingException e ) {
      e.printStackTrace();
      return retval;
    }

    // Set the content type to JSON
    request.setHeader( "content-type", "application/json" );
    request.setHeader( "accept", "application/json" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // get the results of processing the request, should only be 1
    retval = response.getFrame( 0 );

    if ( retval != null ) {
      log.debug( "There are %s fields in the returned record.", retval.size() );
    } else {
      log.error( "No response received from POST call to %s table", snowTable.getName() );
    }
    return retval;
  }




  /**
   * 
   * @throws IOException 
   * @see coyote.snow.worker.InstanceWorker#putRecord(coyote.snow.SnowTable, coyote.snow.SnowRecord)
   */
  @Override
  public DataFrame putRecord( final SnowTable snowTable, final SnowRecord record ) throws IOException {
    DataFrame retval = null;

    log.debug( "Putting record for %s %s", snowTable.getName(), snowTable.getType() );

    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }
    b.append( "api/now/" );
    b.append( snowTable.getType() );
    b.append( "/" );

    if ( snowTable.getName() != null ) {
      b.append( snowTable.getName() );
    } else {
      return retval;
    }

    // Add the system identifier to the URL
    if ( record != null ) {
      final String sysId = record.getAsString( ServiceNow.SYS_ID_FIELD );
      if ( sysId != null ) {
        b.append( "/" );
        b.append( sysId );
      } else {
        return retval;// cannot PUT a record without an ID (must use a POST)
      }
    } else {
      return retval;
    }

    // If we don't want the extra link information, reflect that on the
    // request
    if ( instance.getDefaultParameters().excludeReferenceLinks() ) {
      b.append( "?" );
      b.append( Parameters.EXCLUDE_REFERENCE_LINK );
      b.append( "=true&" );
    }

    // Create the request for the URI
    final HttpPut request = new HttpPut( b.toString() );
    request.setConfig( config );

    try {
      log.debug( "PUT DATA:\r\n%s", record.toString() );

      final HttpEntity entity = new ByteArrayEntity( JSONMarshaler.marshal( record ).getBytes( "utf-8" ) );
      request.setEntity( entity );
    } catch ( final UnsupportedEncodingException e ) {
      e.printStackTrace();
      return retval;
    }

    // Set the content type to JSON
    request.setHeader( "content-type", "application/json" );
    request.setHeader( "accept", "application/json" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // get the results of processing the request, should only be 1
    retval = response.getFrame( 0 );

    if ( retval != null ) {
      log.debug( "There are %s fields in the returned record.", retval.size() );
    } else {
      log.error( "No response record received from PUT call to %s table", snowTable.getName() );
    }
    return retval;
  }

}
