/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow.worker;

import java.io.IOException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.commons.StringUtil;
import coyote.dataframe.DataFrame;
import coyote.snow.Instance;
import coyote.snow.InvocationException;
import coyote.snow.Parameters;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;
import coyote.snow.SnowService;
import coyote.snow.SnowTable;


/**
 * 
 */
public class SoapWorker extends AbstractWorker implements InstanceWorker {

  /** The logger this class uses */
  private static final Logger log = LoggerFactory.getLogger( SoapWorker.class );




  public SoapWorker( Instance instance ) {
    super( instance );
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#getRecords(coyote.snow.Parameters)
   */
  @Override
  public Response getRecords( Parameters params ) throws IOException {
    log.debug( "Request %s", params );
    final StringBuffer b = new StringBuffer( instance.getURI().toString() );
    if ( b.charAt( b.length() - 1 ) != '/' ) {
      b.append( '/' );
    }

    if ( params.getTablename() != null ) {
      b.append( params.getTablename() );
    } else {
      return null;
    }

    b.append( ".do?SOAP" );
    log.debug( "Sending to endpoint:%s", b.toString() );

    params.setOperation( "getRecords" );

    // Create the request for the URI
    final HttpPost request = new HttpPost( b.toString() );
    request.setConfig( config );

    String requestBody = getRequestBody( params );
    StringEntity lEntity = new StringEntity( requestBody );
    request.setEntity( lEntity );

    // Set the content type to XML
    request.setHeader( "content-type", "application/soap+xml" );

    // execute the request
    final Response response = execute( request );

    // throw an exception if we received an error frame
    if ( response.getErrorFrame() != null ) {
      throw new InvocationException( response.getErrorFrame().getAsString( ERROR_MESSAGE_FIELD ) + ": " + response.getErrorFrame().getAsString( ERROR_DETAIL_FIELD ) );
    }

    // return the results of processing the request
    return response;
  }




  /**
   * Create a SOAP request from the given parameters.
   * 
   * @param params
   * 
   * @return the soap envelop as a string
   */
  private String getRequestBody( Parameters params ) {
    StringBuffer b = new StringBuffer();

    b.append( "<soapenv:Envelope xmlns:u=\"http://www.service-now.com/" );
    b.append( params.getTablename() );
    b.append( "\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soapenv:Header/>\r\n  <soapenv:Body>\r\n    <u:" );
    b.append( params.getOperation() );
    b.append( ">\r\n" );

    if ( params.getLimit() > 0 ) {
      b.append( "      <__limit>" );
      b.append( params.getLimit() );
      b.append( "</__limit>\r\n" );
    }

    // Support pagination, use limit and offset to calculate __first_row and __last_row
    if ( params.getOffset() >= 0 ) {
      b.append( "      <__first_row>" );
      b.append( params.getOffset() );
      b.append( "</__first_row>\r\n" );
      if ( params.getLimit() > 0 ) {
        b.append( "      <__last_row>" );
        b.append( params.getLimit() + params.getOffset() );
        b.append( "</__last_row>\r\n" );
      }
    }

    // Use either the raw filter (literal text) or the SnowFilter
    if ( StringUtil.isNotBlank( params.getRawFilter() ) || params.getFilter() != null ) {
      b.append( "      <__encoded_query>" );
      if ( StringUtil.isNotBlank( params.getRawFilter() ) ) {
        b.append( params.getRawFilter() );
      } else {
        b.append( params.getFilter().toString() );
      }
      b.append( "</__encoded_query>\r\n" );
    }

    if ( params.getView() != null && params.getView().trim().length() > 0 ) {
      b.append( "      <__use_view>" );
      b.append( params.getView() );
      b.append( "</__use_view>\r\n" );
    }

    b.append( "    </u:" );
    b.append( params.getOperation() );
    b.append( ">\r\n  </soapenv:Body>\r\n</soapenv:Envelope>" );

    log.debug( "SOAP Request:\r\n%s", b.toString() );

    return b.toString();
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#deleteRecord(coyote.snow.SnowTable, coyote.snow.SnowKey)
   */
  @Override
  public boolean deleteRecord( SnowTable table, SnowKey key ) throws IOException {
    return false;
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#postRecord(coyote.snow.SnowTable, coyote.snow.SnowRecord)
   */
  @Override
  public DataFrame postRecord( SnowTable snowTable, SnowRecord record ) throws IOException {
    // TODO Auto-generated method stub
    return null;
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#putRecord(coyote.snow.SnowTable, coyote.snow.SnowRecord)
   */
  @Override
  public DataFrame putRecord( SnowTable snowTable, SnowRecord record ) throws IOException {
    // TODO Auto-generated method stub
    return null;
  }




  /**
   * @see coyote.snow.worker.InstanceWorker#invoke(coyote.snow.SnowService, coyote.snow.Parameters)
   */
  @Override
  public Response invoke( SnowService snowService, Parameters params ) throws IOException {
    // TODO Auto-generated method stub
    return null;
  }




  @Override
  public DataFrame getRecord( Parameters params ) throws IOException {
    // TODO Auto-generated method stub
    return null;
  }

}
