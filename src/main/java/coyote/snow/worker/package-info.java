/**
 * Instance workers handle all the details of interacting with the ServiceNow 
 * instance through web service calls and populating the API model.
 * 
 * <p>All protocol details (JSON REST, SOAP, XML REST, etc.) are encapsulated 
 * in this package.</p>
 */
package coyote.snow.worker;