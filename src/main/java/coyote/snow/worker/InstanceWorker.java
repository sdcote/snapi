/*
 *
 */
package coyote.snow.worker;

import java.io.Closeable;
import java.io.IOException;

import coyote.dataframe.DataFrame;
import coyote.snow.Parameters;
import coyote.snow.SnowKey;
import coyote.snow.SnowRecord;
import coyote.snow.SnowService;
import coyote.snow.SnowTable;


/**
 * This is the basic contract all workers must support.
 * 
 * <p>It is expected that there will be multiple types of workers. The first 
 * worker implemented is a REST service worker which uses JSON. Other protocols 
 * may follow if the enterprise has constraints in place governing the types of 
 * web services supported. For example, it is possible that a company may only 
 * support SOAP services and a SOAP worker will have to be used.</p>
 */
public interface InstanceWorker extends Closeable {

  /**
   * 
   * @param params parameters for the request.
   * 
   * @return the list of records matching the given parameters or a record containing error information
   * 
   * @throws IOException 
   */
  Response getRecords( Parameters params ) throws IOException;




  /**
   * @param params parameters for the request.
   * 
   * @return the record matching the given parameters or a record containing error information
   * 
   * @throws IOException 
   */
  DataFrame getRecord( Parameters params ) throws IOException;




  /**
   * Delete the record with the system identifier (sys_id) stored in the given 
   * request parameters.
   * 
   * @param table the table from which the record is to be deleted
   * @param key the unique key (system identifier) of the record to delete
   * 
   * @return true if the record was deleted, false if not (e.g. not found) 
   * 
   * @throws IOException 
   */
  boolean deleteRecord( SnowTable table, SnowKey key ) throws IOException;




  /**
   * Create a record in the table
   * 
   * @param snowTable
   * @param record
   * 
   * @return the current state of the record as it exists in the table to which it was added or a record containing error information
   * 
   * @throws IOException 
   */
  DataFrame postRecord( SnowTable snowTable, SnowRecord record ) throws IOException;




  /**
   * Modify an existing record with the data in the given record.
   * 
   * <p>The given record MUST have the {@code sys_id} field set for the 
   * worker to put the data in the appropriate record.
   * 
   * @param snowTable
   * @param record
   * 
   * @return the current state of the record as it exists in the table to which it was updated or a record containing error information
   * 
   * @throws IOException 
   */
  public DataFrame putRecord( SnowTable snowTable, SnowRecord record ) throws IOException;




  /**
   * Call an in-bound web service using the provided parameters.
   * 
   * @param snowService the reference to the SnowService describing the details of the service endpoint.
   * @param params parameters for the request.
   * 
   * @return the response of the service request.
   * @throws IOException 
   */
  Response invoke( SnowService snowService, Parameters params ) throws IOException;

}
