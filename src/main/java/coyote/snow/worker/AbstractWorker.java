/*
 *
 */
package coyote.snow.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpHost;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.dataframe.DataField;
import coyote.dataframe.DataFrame;
import coyote.dataframe.marshal.JSONMarshaler;
import coyote.snow.Instance;
import coyote.snow.worker.decorator.BasicAuth;
import coyote.snow.worker.decorator.RequestDecorator;


/**
 * This is the base class for all workers.
 */
public abstract class AbstractWorker implements InstanceWorker {
  protected Instance instance;

  // This is the persistent http client we will use to send all our requests
  protected CloseableHttpClient httpClient;

  // Create a context in which we will execute our request
  protected final HttpClientContext localContext = HttpClientContext.create();

  // HTTP Client configuration settings
  protected RequestConfig config;

  // The host of our targeted instance
  protected HttpHost target = null;

  // If we perform preemptive authorization, use a decorator
  private BasicAuth preemptiveAuth = null;

  private static final String RESULT_FIELD = "result";
  private static final String ERROR_FIELD = "error";
  private static final String STATUS_FIELD = "status";

  protected static final String ERROR_MESSAGE_FIELD = "message";
  protected static final String ERROR_DETAIL_FIELD = "detail";

  protected static final String QUERY = "sysparm_query";
  protected static final String VIEW = "sysparm_view";
  protected static final String SYS_ID = "sysparm_sys_id";
  protected static final String RECORD_COUNT = "sysparm_record_count";
  protected static final String ACTION = " sysparm_action";
  protected static final String DISPLAY_VALUE = "  sysparm_display_value";
  protected static final String DISPLAY_VARIABLES = " displayvariables";
  protected static final String FIELDS = "sysparm_fields";
  protected static final String LIMIT = "sysparm_limit";
  protected static final String EXCLUDE_REFERENCE_LINK = "sysparm_exclude_reference_link";

  /** The logger this class uses */
  protected static final Logger log = LoggerFactory.getLogger( AbstractWorker.class );




  /**
   * This is is the default constructor for all workers.
   * 
   * @param instance
   */
  public AbstractWorker( Instance instance ) {
    this.instance = instance;

    // This lays open several components of the underlying HTTP client library
    // so as to allow configuration of many different aspects of the connection
    // process

    // Define and configure the Connection Manager
    PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager();
    connMgr.closeIdleConnections( 15, TimeUnit.MINUTES );

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // our own keep-alive strategy
    ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
      public long getKeepAliveDuration( HttpResponse response, HttpContext context ) {
        // Honor 'keep-alive' header
        HeaderElementIterator it = new BasicHeaderElementIterator( response.headerIterator( HTTP.CONN_KEEP_ALIVE ) );
        while ( it.hasNext() ) {
          HeaderElement he = it.nextElement();
          String param = he.getName();
          String value = he.getValue();
          if ( value != null && param.equalsIgnoreCase( "timeout" ) ) {
            try {
              return Long.parseLong( value ) * 1000;
            } catch ( NumberFormatException ignore ) {}
          }
        }
        // HttpHost target = (HttpHost)context.getAttribute( HttpClientContext.HTTP_TARGET_HOST );
        // we could also check for other parameters such as hostname and return 
        // a different value for each server or domain.
        // for right now just keep it at 10 minutes (600 seconds) for long queries
        return 600 * 1000;
      }
    };
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // Define and configure the client
    httpClient = HttpClientBuilder.create().setConnectionManager( connMgr ).setKeepAliveStrategy( myStrategy ).build();

    // Setup our HTTP fixtures
    target = new HttpHost( instance.getHost(), instance.getPort(), instance.getScheme() );

    // Now set timeouts
    int soTimeout = 15 * 60 * 1000;
    int connTimeout = 15 * 60 * 1000;
    int rqTimeout = 15 * 60 * 1000;

    // If there are both proxy host and port values, configure the request
    // to use them
    if ( instance.getProxySettings() != null ) {
      final HttpHost proxy = new HttpHost( instance.getProxySettings().getHost(), instance.getProxySettings().getPort() );
      config = RequestConfig.copy( RequestConfig.DEFAULT ).setSocketTimeout( soTimeout ).setConnectTimeout( connTimeout ).setConnectionRequestTimeout( rqTimeout ).setProxy( proxy ).build();
      log.debug( "Connecting to instance " + target + " via " + proxy );
    } else {
      config = RequestConfig.copy( RequestConfig.DEFAULT ).setSocketTimeout( soTimeout ).setConnectTimeout( connTimeout ).setConnectionRequestTimeout( rqTimeout ).build();
      log.debug( "Connecting to instance " + target );
    }

    // If we have credentials, set them in the local client context
    if ( ( instance.getUser() != null ) && ( instance.getPassword() != null ) ) {
      final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

      // If we have proxy credentials, add them
      if ( instance.getProxySettings() != null ) {
        // NTLM Credentials
        if ( isNotBlank( instance.getProxySettings().getDomain() ) ) {
          credentialsProvider.setCredentials( new AuthScope( instance.getProxySettings().getHost(), instance.getProxySettings().getPort(), null, "NTLM" ), new NTCredentials( instance.getProxySettings().getUsername(), instance.getProxySettings().getPassword(), null, instance.getProxySettings().getDomain() ) );
          log.debug( "Adding NTLM credential support for %s", instance.getProxySettings().getUsername() );
        }
      }

      // Now set the credentials for the target host:port - Do not specify
      // a scheme...it results in a 401
      credentialsProvider.setCredentials( new AuthScope( target.getHostName(), target.getPort() ), new UsernamePasswordCredentials( instance.getUser(), instance.getPassword() ) );
      log.debug( "Adding basic auth credential support for %s", instance.getUser() );

      // place the credentials provider in the client context
      localContext.setCredentialsProvider( credentialsProvider );

    }

  }




  /**
   * Execute the request over the current connections and return the results.
   * 
   * <p>Only one record is expected to be returned.</p>
   * 
   * <p>This method makes all calls uniform in nature.</p>
   * 
   * @param request
   * 
   * @return a dataframe containing the results
   * @throws IOException 
   */
  protected Response execute( final HttpRequest request ) {
    final Response response = new Response( request );

    // Add any last minute headers to the request
    setHeaders( request );

    // Try to get proxies to honor keep-alives for long running processes
    //request.setHeader( "Keep-Alive", "timeout=900" );
    //request.setHeader( "Connection" , "Keep-Alive");

    // if we are to authenticate preemptively,
    if ( instance.isAuthenticatingPreemptively() ) {
      //  make sure we have a BasicAuth decorator
      if ( preemptiveAuth == null ) {
        preemptiveAuth = new BasicAuth();
        preemptiveAuth.setUsername( instance.getUser() );
        preemptiveAuth.setPassword( instance.getPassword() );
      }
      // and call it to add the basic auth header to all requests
      preemptiveAuth.process( request );
    }

    response.transactionStart();
    response.requestStart();

    // Execute the request
    try (CloseableHttpResponse httpResponse = httpClient.execute( target, request, localContext )) {
      response.requestEnd();

      final int status = httpResponse.getStatusLine().getStatusCode();
      response.setStatusCode( status );
      response.setStatusPhrase( httpResponse.getStatusLine().getReasonPhrase() );

      // Debug messages
      if ( log.isDebugEnabled() ) {
        log.debug( "Request:\r\n    %s\r\nResponse:\r\n    %s", request.toString(), httpResponse.getStatusLine().toString() );
        if ( ( status >= 200 ) && ( status < 300 ) ) {
          log.debug( "Success - %s", httpResponse.getStatusLine().toString() );
        } else if ( ( status >= 300 ) && ( status < 400 ) ) {
          log.debug( "Unexpected Response - %s", httpResponse.getStatusLine().toString() );
        } else if ( ( status >= 400 ) && ( status < 500 ) ) {
          log.debug( "Access error - %s", httpResponse.getStatusLine().toString() );
        } else if ( status >= 500 ) {
          log.debug( "Server error - %s", httpResponse.getStatusLine().toString() );
        }
      }

      // This is only applicable for GET requests which may return multiple results
      Header[] headers = httpResponse.getHeaders( "X-Total-Count" );
      if ( headers != null && headers.length > 0 ) {
        Header total = headers[0];
        try {
          response.setTotalCount( Integer.parseInt( total.getValue() ) );
        } catch ( NumberFormatException e ) {
          log.warn( "Server returned a non-numeric value of '%s' for total count", total.getValue() );
        }
        log.debug( "Total result set size: %s records", response.getTotalCount() );
      } else {
        if ( ( status >= 200 ) && ( status < 300 ) && request instanceof HttpGet ) {
          log.debug( "Total count was not received: assuming 1" );
          response.setTotalCount( 1 );
        }
      }

      // This can appear in any response
      headers = httpResponse.getHeaders( "X-TRANSACTION-TIME-MS" );
      if ( headers != null && headers.length > 0 ) {
        Header total = headers[0];
        try {
          response.setServerTime( Integer.parseInt( total.getValue() ) );
        } catch ( NumberFormatException e ) {
          log.warn( "Server returned a non-numeric value of '%s' for transaction time", total.getValue() );
        }
        log.debug( "Transaction time reported by server as %s milliseconds", response.getTotalCount() );
      }

      // Status of a 301 or a 302, look for a Location: header in the response and use that URL
      if ( status >= 300 && status < 400 ) {
        response.setLink( httpResponse.getFirstHeader( "Location" ).getValue() );
      }

      // Check for a body
      if ( httpResponse.getEntity() != null ) {

        // get the body as a string
        final String body = EntityUtils.toString( httpResponse.getEntity(), "UTF-8" );

        log.debug( "Marshaling response body of '%s%s", body.substring( 0, body.length() > 500 ? 500 : body.length() ), body.length() <= 500 ? "'" : " ...'" );

        // Parse the body into frames
        List<DataFrame> frames;
        response.parseStart();
        try {
          frames = null;
          if ( Instance.isREST() ) {
            log.debug( "Marshaling a JSON response of %s characters", body.length() );
            frames = JSONMarshaler.marshal( body );
          } else {
            log.debug( "Marshaling a SOAP response of %s characters", body.length() );
            frames = marshalSOAP( body ); // replace with DataFrame Marshaler
          }
        } catch ( Exception e ) {
          throw e;
        }
        finally {
          response.parseEnd();
        }

        // Because is is possible for responses to have multiple set of data, 
        // make sure just to retrieve the first full frame of data
        if ( frames != null && frames.size() > 0 ) {
          if ( frames.size() > 1 ) {
            log.error( "The response contained more than one object - only using first response object" );
          }
          final DataFrame responseFrame = frames.get( 0 );

          final DataFrame results = (DataFrame)responseFrame.getObject( RESULT_FIELD );
          response.setErrorFrame( (DataFrame)responseFrame.getObject( ERROR_FIELD ) );
          response.setStatus( responseFrame.getAsString( STATUS_FIELD ) );

          if ( results != null ) {

            // Multiple results come as an array, single results are their own frame
            if ( results.isArray() ) {
              for ( final DataField field : results.getFields() ) {
                if ( field.isFrame() ) {
                  response.add( (DataFrame)field.getObjectValue() );
                } else {
                  log.warn( "Malformed response: array of records contained a %s field: %s ", field.getTypeName(), field.toString() );
                }
              }
            } else {
              // This is a single result, add it to the return value
              response.add( results );
            }

          } else {
            log.debug( "RESPONSE: NO RESPONSE DATA RETURNED" );
          }

        } else {
          log.debug( "There were no valid frames in the response body" );
        }

      } else {
        log.debug( "The response did not contain a body" );
      }

    } catch ( ClientProtocolException e1 ) {
      response.requestEnd();
      DataFrame errorFrame = new DataFrame();
      errorFrame.add( ERROR_MESSAGE_FIELD, "Protocol Error" );
      errorFrame.add( ERROR_DETAIL_FIELD, e1.getMessage() );
      response.setErrorFrame( errorFrame );
      e1.printStackTrace();
      log.error( e1.getMessage() );
    } catch ( IOException e1 ) {
      response.requestEnd();
      DataFrame errorFrame = new DataFrame();
      errorFrame.add( ERROR_MESSAGE_FIELD, "IO Error" );
      errorFrame.add( ERROR_DETAIL_FIELD, e1.getMessage() );
      response.setErrorFrame( errorFrame );
      e1.printStackTrace();
      log.error( e1.getMessage() );
    }
    finally {
      response.transactionEnd();
    }

    return response;
  }




  private List<DataFrame> marshalSOAP( String body ) {
    List<DataFrame> retval = new ArrayList<DataFrame>();

    // there is only one response
    DataFrame response = new DataFrame();
    retval.add( response );

    if ( body != null ) {
      try {
        SoapResponse soap = new SoapResponse( body );

        if ( soap.isFault() ) {
          log.error( "Error reported: %s - %s  Details: %s", soap.getFaultCode(), soap.getFaultString(), soap.getFaultDetail() );
          log.debug( "Fault Body: %s", body );
          response.put( ERROR_FIELD, soap.getFaultCode() + " - " + soap.getFaultString() + " : " + soap.getFaultDetail() );
        } else {
          DataFrame results = new DataFrame();
          for ( DataFrame frame : soap.getResultFrames() ) {
            results.add( frame );
          }

          // Put all the result frames in the response field
          response.put( RESULT_FIELD, results );
        }

      } catch ( SoapParseException e ) {
        log.error( "Problems parsing SOAP response body of '%s...'", body.substring( 0, body.length() > 500 ? 500 : body.length() ) );
        log.error( "Could not marshal SOAP response: %s", e.getMessage() );
      }
    }
    return retval;
  }




  /**
   * Checks if a string is not null, empty ("") and not only whitespace.
   * 
   * @param str the String to check, may be null
   * 
   * @return <code>true</code> if the String is not empty and not null and not
   *         whitespace
   */
  protected static boolean isBlank( final String str ) {
    int strLen;
    if ( ( str == null ) || ( ( strLen = str.length() ) == 0 ) ) {
      return true;
    }
    for ( int i = 0; i < strLen; i++ ) {
      if ( ( Character.isWhitespace( str.charAt( i ) ) == false ) ) {
        return false;
      }
    }
    return true;
  }




  /**
   * Checks if a string is not null, empty ("") and not only whitespace.
   * 
   * <p>This is a convenience wrapper around isBlank(String) to make code 
   * slightly more readable.</p>
   * 
   * @param str the String to check, may be null
   * 
   * @return <code>true</code> if the String is not empty and not null and not
   *         whitespace
   * 
   * @see #isBlank(String)
   */
  protected static boolean isNotBlank( final String str ) {
    return !isBlank( str );
  }




  @Override
  public void close() throws IOException {
    httpClient.close();
  }




  /**
   * This method enriches the given HTTP Request with any additional headers
   * required by the SOA environment.
   * 
   * <p>This will modify the given request object.</p>
   * 
   * @param request the request to query and update if necessary.
   */
  public void setHeaders( HttpMessage request ) {
    for ( RequestDecorator decorator : instance.getRequestDecorators() ) {
      decorator.process( request );
    }
  }

}
