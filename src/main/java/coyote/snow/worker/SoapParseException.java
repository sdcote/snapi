/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow.worker;

import coyote.commons.StringParseException;


/**
 * 
 */
public class SoapParseException extends StringParseException {

  private static final long serialVersionUID = 8930402440302579292L;




  /**
   * Create an exception with the given message
   * 
   * @param msg message describing the exception
   */
  public SoapParseException( String msg ) {
    super( msg );
  }




  /**
   * Create an exception with the given message and the context in which it 
   * occurred.
   * 
   * @param msg message describing the exception
   * @param position the context in which the exception occurred.
   */
  public SoapParseException( String msg, String position ) {
    super( msg, position );
  }

}
