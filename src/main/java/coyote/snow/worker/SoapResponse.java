/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.commons.SimpleReader;
import coyote.commons.StringParser;
import coyote.dataframe.DataField;
import coyote.dataframe.DataFrame;


/**
 * If the Response is a Fault 
 */
public class SoapResponse extends StringParser {

  private static final Logger log = LoggerFactory.getLogger( SoapResponse.class );

  private final String rawBody;
  private static final int OPEN = (int)'<';
  private static final int CLOSE = (int)'>';
  private static final int QM = (int)'?';
  private static final int SLASH = (int)'/';

  private static final String ENVELOPE = "Envelope";
  private static final String BODY = "Body";
  private static final String FAULT = "Fault";

  private static final String FAULTCODE = "faultcode";
  private static final String FAULTSTRING = "faultstring";
  private static final String DETAIL = "detail";
  private static final String CODE = "Code";
  private static final String REASON = "Reason";

  private boolean faultFlag = false;
  private String faultDetail = null;
  private String faultString = null;
  private String faultCode = null;

  //
  List<String> stack = new ArrayList<String>();




  /**
   * This wraps the SOAP response body in an XML parser and reads to the body 
   * of the response.
   * 
   * <p>The {@link #getResultFrames()} method can then be called to retrieve 
   * the records (in the form of Data Frames) from the response.</p>
   * 
   * <p>If the response represents a SOAP fault, this method will attempt to 
   * parse the two known forms of SOAP faults returned from ServiceNow. The 
   * fault data will then be accessible via {@link #isFault()}, 
   * {@link #getFaultCode()}, {@link #getFaultDetail()}, and 
   * {@link #getFaultString()}.</p> 
   *  
   * @param body the body of the response message
   * 
   * @throws SoapParseException if parsing failed for some reason
   */
  public SoapResponse( String body ) throws SoapParseException {
    // init parsing with a simple reader on the given body with XML delimiters
    super( new SimpleReader( body ), " \t\n><" );
    rawBody = body;

    Tag tag = null;

    // Start reading tags until we reach the envelope
    do {
      tag = readTag();

      if ( tag == null )
        break;
    }
    while ( !BODY.equalsIgnoreCase( tag.getName() ) );

    if ( tag == null ) {
      throw new SoapParseException( "No SOAP Body found", super.getPosition() );
    }

    // Read either a Fault or a Response object
    tag = readTag();

    if ( FAULT.equalsIgnoreCase( tag.getName() ) ) {
      faultFlag = true;
      do {
        tag = readTag();
        if ( DETAIL.equalsIgnoreCase( tag.getName() ) && !tag.isEmptyTag() ) {
          faultDetail = readValue();
        } else if ( FAULTSTRING.equalsIgnoreCase( tag.getName() ) && !tag.isEmptyTag() ) {
          faultString = readValue();
        } else if ( FAULTCODE.equalsIgnoreCase( tag.getName() ) && !tag.isEmptyTag() ) {
          faultCode = readValue();
        } else if ( CODE.equalsIgnoreCase( tag.getName() ) && !tag.isEmptyTag() ) {
          tag = readTag();// should be "env:Value"
          faultCode = readValue();
          if ( faultCode != null ) {
            faultCode = faultCode.trim();
          }
          tag = readTag();// should be the close of "env:Value"
        } else if ( REASON.equalsIgnoreCase( tag.getName() ) && !tag.isEmptyTag() ) {
          tag = readTag();// should be "env:Text"
          faultString = readValue();
          if ( faultString != null ) {
            faultString = faultString.trim();
          }
          tag = readTag();// should be the close of "env:Text"
        }
      }
      while ( tag != null && !FAULT.equalsIgnoreCase( tag.getName() ) );
    } else {
      log.debug( "Processing response of %s", tag.getName() );
    }

  }




  private String readValue() {
    StringBuffer b = new StringBuffer();
    try {
      if ( OPEN == peek() )
        return null;

      // keep reading characters until the next character is an open marker
      while ( OPEN != peek() ) {
        b.append( (char)read() );
      }

    } catch ( IOException e ) {
      e.printStackTrace();
    }
    return b.toString();
  }




  private Tag readTag() {
    Tag retval = null;
    String token = null;

    try {
      // read to the next open character
      readTo( OPEN );

      // read everything up to the closing character into the token
      token = readTo( CLOSE );
      //log.debug( "Read tag of '%s'", token );

      if ( token != null ) {
        token = token.trim();

        if ( token.length() > 0 ) {
          retval = new Tag();
          String name = null;

          if ( token.endsWith( "/" ) ) {
            retval.setEmptyTag( true );
            token = token.substring( 0, token.length() - 1 );
          }
          if ( token.startsWith( "/" ) ) {
            retval.setEndTag( true );
            token = token.substring( 1 );
          }

          // See if there are attributes
          if ( token.indexOf( ' ' ) > -1 ) {
            name = token.substring( 0, token.indexOf( ' ' ) );
          } else {
            name = token;
          }

          // split the name into namespace and name
          if ( name.indexOf( ':' ) > -1 ) {
            retval.setNamespace( name.substring( 0, token.indexOf( ':' ) ) );
            retval.setName( name.substring( token.indexOf( ':' ) + 1 ) );
          } else {
            retval.setName( name );
          }

        }
      }
    } catch ( IOException e ) {
      //e.printStackTrace();
    }

    return retval;
  }




  /**
   * @return the fault details in this response
   * 
   * @see #isFault()
   */
  public String getFaultDetail() {
    return faultDetail;
  }




  /**
   * @return the fault string (message) in this response
   * 
   * @see #isFault()
   */
  public String getFaultString() {
    return faultString;
  }




  /**
   * @return the fault code in this response
   * 
   * @see #isFault()
   */
  public String getFaultCode() {
    return faultCode;
  }




  /**
   * Indicates if this response is an error or not.
   * 
   * @return true, this response represents an error occurred, false this is a 
   *         normal response with data.
   *         
   * @see #getFaultCode()
   * @see #getFaultDetail()
   * @see #getFaultString()
   */
  public boolean isFault() {
    return faultFlag;
  }




  public List<DataFrame> getResultFrames() {
    List<DataFrame> retval = new ArrayList<DataFrame>();

    // The constructor should have placed the parser in the response ready to 
    // parse through all the results.

    Tag tag = null;

    // Start reading tags until we reach the body
    do {
      tag = readTag();

      if ( tag != null ) {
        if ( tag.isOpenTag() ) {
          stack.add( tag.getName() );
          DataFrame frame = getFrame();

          if ( frame != null ) {
            retval.add( frame );
          }

          // read the closing tag
          //tag = readTag();
          //if ( tag.isOpenTag() ) { log.error( "Unexpected open tag %s at %s", tag.getName(), getPosition() ); }

          if ( tag.getName().equals( stack.get( stack.size() - 1 ) ) ) {
            stack.remove( stack.size() - 1 );
          }
        } else {
          if ( stack.size() > 0 ) {
            log.error( "Unexpected close tag %s at %s - Expecting:%s ", tag.getName(), getPosition(), stack.get( stack.size() - 1 ) );
          } else {
            // another close tag and our stack is empty means this level is done
            break;
          }
        }
      } else {
        break; // null tag implies no more data
      }

    }
    while ( !BODY.equalsIgnoreCase( tag.getName() ) );

    log.debug( "Returning %s results", retval.size() );
    return retval;
  }




  /**
   * @return a dataframe populated from the data at the current position in the parsing.
   */
  private DataFrame getFrame() {
    DataFrame retval = new DataFrame();
    Tag tag = null;
    String value = null;

    // read the open tag
    tag = readTag();

    while ( tag.isOpenTag() ) {
      DataField field = null;
      //log.debug( "Field:%s", tag.getName() );
      if ( tag.isNotEmptyTag() ) {
        value = readValue();
        //log.debug( "Value:%s", value );
        field = new DataField( tag.getName(), value );

        //log.debug( "Added Field %s at %s", field.toString(), getPosition() );
        // ad the field to the frame
        retval.add( field );

        // this should be the closing tag 
        tag = readTag();

        // an open tag implies nested data
        if ( tag.isOpenTag() ) {
          log.error( "Nested tag '%s' at %s", tag.getName(), getPosition() );
          // read to the close of the current tag 
          try {
            readToPattern( "</" + field.getName() );
          } catch ( IOException e ) {
            e.printStackTrace();
          }
        }

      } else {
        //log.debug( "Value: EMPTY" );
        field = new DataField( tag.getName(), null );
      }
      // Now read the next tag, it should be an open tag for the next field
      tag = readTag();
    }

    return retval;
  }

  //

  //

  //

  //

  /**
   * 
   */
  private class Tag {
    private String name = null;
    private String namespace = null;
    private boolean closetag = false;
    private boolean emptytag = false;




    /**
     * @return the name
     */
    public String getName() {
      return name;
    }




    public boolean isCloseTag() {
      return closetag;
    }




    public boolean isOpenTag() {
      return !closetag;
    }




    /**
     * @param name the name to set
     */
    public void setName( String name ) {
      this.name = name;
    }




    /**
     * @return the namespace
     */
    public String getNamespace() {
      return namespace;
    }




    /**
     * @param namespace the namespace to set
     */
    public void setNamespace( String namespace ) {
      this.namespace = namespace;
    }




    /**
     * @param flag true to represent the tag as a terminator
     */
    public void setEndTag( boolean flag ) {
      closetag = flag;
    }




    /**
     * @return true if there is a value following this tag, false if it is empty
     */
    public boolean isEmptyTag() {
      return emptytag;
    }




    public boolean isNotEmptyTag() {
      return !emptytag;
    }




    /**
     * @param flag true, the tag is empty, false, the tag is followed by a value and an end tag.
     */
    public void setEmptyTag( boolean flag ) {
      emptytag = flag;
    }

  }

}
