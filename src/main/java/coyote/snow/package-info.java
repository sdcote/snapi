/**
 * Provides an API to access ServiceNow tables using JSON Web Services.
 * 
 * <ol><li>Create a role containing the following roles:<ul>
 *     <li>rest_service</li></ul></li>
 *   <li>If you want to use schemas, grant the role read access to the 
 *   following tables:<ul>
 *     <li>sys_db_object</li>
 *     <li>sys_dictionary</li></ul>
 *   <li>Ensure that the role has read access to any tables with which you 
 *     intend to work. Be sure to check for field level Access Controls which 
 *     could interfere with operation.</li>
 *   <li>Create a ServiceNow user and</li>
 *   <li>Assign the above role to the user.</li></ol>
 */
package coyote.snow;