/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

import java.io.IOException;
import java.util.List;

import coyote.dataframe.DataFrame;
import coyote.snow.worker.Response;


/**
 * Models a simple service endpoint hosted by the instance.
 * 
 * <p>Instances of this class provide a simple facade to in-bound services 
 * hosted by the instance such as scripted web services.</p> 
 * 
 * <p><strong>SSO authentication such as SAML is NOT supported.</strong></p>
 */
public class SnowService {

  final String servicename;
  private final Instance instance;




  /**
   * Default constructor called by the instance
   * 
   * @param instance the reference to the instance hosting this service
   * @param name the name of this service
   */
  SnowService( Instance instance, String name ) {
    this.servicename = name;
    this.instance = instance;
  }




  /**
   * @return the name of this service
   */
  public String getName() {
    return servicename;
  }




  /**
   * @return the reference to the instance hosting this service
   */
  public Instance getInstance() {
    return instance;
  }




  public SnowRecordList invoke( final Parameters params ) throws IOException {

    if ( params != null ) {

      // Get the response data, in it contains error and other information
      final Response response = instance.getWorker().invoke( this, params );

      // Get a list of Abstract Data Type objects from the worker representing 
      // the records
      final List<DataFrame> records = response.getResults();

      final SnowRecordList list = new SnowRecordList( records.size() );
      for ( final DataFrame record : records ) {
        final SnowRecord rec = new SnowRecord( null, record );
        list.add( rec );
      }
      return list;
    } else {
      // no parameters = no records returned
      return new SnowRecordList();
    }
  }
}
