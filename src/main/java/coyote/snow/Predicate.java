/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

/**
 * 
 */
public enum Predicate {
  CONTAINS( "LIKE"), 
  DIFFERENT_FROM( "NSAMEAS"), 
  DOES_NOT_CONTAIN( "NOT LIKE"), 
  ENDS_WITH( "ENDSWITH"), 
  GREATER_THAN( ">"), 
  GREATER_THAN_EQUALS( ">="), 
  IN( "IN"), 
  IS( "="), 
  IS_ANYTHING( "ANYTHING"), 
  IS_EMPTY( "ISEMPTY"), 
  IS_EMPTY_STRING( "EMPTYSTRING"), 
  IS_NOT( "!="), 
  IS_NOT_EMPTY( "ISNOTEMPTY"), 
  KEYWORDS( "123TEXTQUERY321"), 
  LESS_THAN( "<"), 
  LESS_THAN_EQUALS( "<="), 
  SAME_AS( "SAMEAS"), 
  STARTS_WITH( "STARTSWITH");

  private String value;




  private Predicate( final String value ) {
    this.value = value;
  }




  /**
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
    return value;
  }




  /**
   * @return true if the predicate requires a value argument
   */
  public boolean requiresValue() {
    if ( value.equalsIgnoreCase( "ISEMPTY" ) || value.equalsIgnoreCase( "ANYTHING" ) || value.equalsIgnoreCase( "EMPTYSTRING" ) || value.equalsIgnoreCase( "ISNOTEMPTY" ) ) {
      return false;
    }
    return true;
  }

}
