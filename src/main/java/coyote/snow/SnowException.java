package coyote.snow;

import java.io.IOException;


/**
 * Exception thrown when there is problem decoding a web service response data.
 */
public class SnowException extends IOException {

  private static final long serialVersionUID = 1596481827091968120L;




  public SnowException( final SnowTable table, final Exception cause, final String response ) {
    super( response + "\ntable=" + table.getName(), cause );
  }




  public SnowException( final SnowTable table, final String message, final String response ) {
    super( response == null ? message + "\ntable=" + table.getName() : message + "\ntable=" + table.getName() + "\n" + response );
  }
}
