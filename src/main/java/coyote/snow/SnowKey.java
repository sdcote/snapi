package coyote.snow;

/**
 * Thin wrapper for a sys_id (GUID).
 */
public class SnowKey {

  String value;

protected SnowKey(){}


  public SnowKey( final String value ) {
    if ( ( value == null ) || ( value.trim().length() != 32 ) ) {
      throw new IllegalArgumentException( "bad key value" );
    }
    this.value = value.trim();
  }




  @Override
  public String toString() {
    return value;
  }
}
