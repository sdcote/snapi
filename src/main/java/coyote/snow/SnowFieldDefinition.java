package coyote.snow;

/**
 * Contains data type information for a single field in a SNow table.
 */
public class SnowFieldDefinition {

  private final String name;
  private final String label;
  private final String type;
  private final int max_length;
  private final String ref_table;
  private final boolean displayValue;

  private String help = null;
  private String hint = null;

  private static final String ELEMENT = "element";
  private static final String LABEL = "column_label";
  private static final String TYPE = "internal_type";
  private static final String LENGTH = "max_length";
  private static final String REFERENCE = "reference";
  private static final String DISPLAY = "display";




  public SnowFieldDefinition( final SnowTable table, final SnowRecord dictionary ) {

    name = dictionary.getFieldValue( ELEMENT );
    label = dictionary.getFieldValue( LABEL );
    type = dictionary.getFieldValue( TYPE );
    max_length = dictionary.getFieldAsInt( LENGTH );
    ref_table = dictionary.getFieldValue( REFERENCE );
    displayValue = dictionary.getFieldAsBoolean( DISPLAY );
  }




  private SnowFieldDefinition( final String name, final boolean dv ) {
    this.name = name;
    label = "";
    type = "string";
    max_length = 255;
    ref_table = "";
    displayValue = dv;
  }




  public SnowFieldDefinition displayValueDefinition() {
    if ( isReference() ) {
      final SnowFieldDefinition result = new SnowFieldDefinition( "dv_" + name, true );
      return result;
    } else {
      return null;
    }
  }




  /**
   * @return the help text for the field or empty string ("") if not set; never returns null.
   */
  public String getHelp() {
    return help == null ? "" : help;
  }




  /**
   * @return the hint text for the field or empty string ("") if not set; never returns null.
   */
  public String getHint() {
    return hint == null ? "" : hint;
  }




  /**
   * @return the label used in the UI
   */
  public String getLabel() {
    return label;
  }




  /**
   * @return the maximum length the field can hold
   */
  public int getLength() {
    return max_length;
  }




  public String getName() {
    return name;
  }




  public String getReference() {
    return ref_table;
  }




  public String getType() {
    return type;
  }




  public boolean isDisplayValue() {
    return displayValue;
  }




  public boolean isReference() {
    return ( ( ref_table != null ) && ( ref_table.length() > 0 ) );
  }




  /**
   * Set the help text for the filed.
   * 
   * @param text the text which is displayed in the UI when help is requested for filling in the field.
   */
  public void setHelp( final String text ) {
    help = text;
  }




  /**
   * The the hint text for this field.
   *  
   * @param text the hit that is displayed in the UI when the mouse hovers over the field.
   */
  public void setHint( final String text ) {
    hint = text;
  }




  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer b = new StringBuffer( "FieldDef: " );
    b.append( getName() );
    b.append( " " );
    b.append( getType() );
    b.append( "(" );
    b.append( this.getLength() );
    b.append( ") " );
    return b.toString();
  }
}
