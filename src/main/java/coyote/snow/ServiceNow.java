package coyote.snow;

/**
 * Holds the constants for this API
 */
public class ServiceNow {

  public static final String SYS_DB_OBJ = "sys_db_object";
  public static final String SYS_DICTIONARY = "sys_dictionary";
  public static final String SYS_DOCUMENTATION = "sys_documentation";
  public static final String ECC_QUEUE = "ecc_queue";

  public static final String SYS_ID_FIELD = "sys_id";

  public static final String SYS_UPDATED_ON_FIELD = "sys_updated_on";
  public static final String SYS_CREATED_ON_FIELD = "sys_created_on";

  public static final String SYS_CREATED_BY_FIELD = "sys_created_by";
  public static final String SYS_MOD_COUNT_FIELD = "sys_mod_count";
  public static final String SYS_UPDATED_BY_FIELD = "sys_updated_by";
}
