package coyote.snow;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.commons.StringUtil;
import coyote.snow.worker.InstanceWorker;
import coyote.snow.worker.JsonWorker;
import coyote.snow.worker.SoapWorker;
import coyote.snow.worker.decorator.RequestDecorator;


/**
 * This class contains connection information for a ServiceNow instance, and 
 * will also be used cache table definitions.
 * 
 * <p>When instantiated, this class will read sys_dictionary and sys_db_object.
 * An exception will be thrown if these tables cannot be read.</p>
 */
public class Instance implements Closeable {

  URI baseurl;
  String username;
  String password;
  Hashtable<String, SnowTable> tablemap = new Hashtable<String, SnowTable>();
  Hashtable<String, SnowService> servicemap = new Hashtable<String, SnowService>();

  // optional decorators which enrich the requests before they are submitted.
  private final List<RequestDecorator> requestDecorators = new ArrayList<RequestDecorator>();

  boolean isUsingSchemas = false;
  boolean validate = true;
  private final SnowTable dictionary;
  private final SnowTable tables;
  private final SnowTable documentation;
  Logger log = LoggerFactory.getLogger( getClass() );

  private final InstanceWorker worker;
  private final Parameters defaultParameters = new Parameters();
  private static boolean useReST = true;
  private volatile boolean authenticatePreemptively = false;

  /** Any proxy settings this instance should use */
  private final Proxy proxy;




  /**
   * Simple constructor which specifies no proxy.
   * 
   * @param url Complete URL to the physical instance
   * @param user ServiceNow user name to use 
   * @param pass Password credential for the user
   * 
   * @throws IOException if the URL is invalid
   */
  public Instance( final String url, final String user, final String pass ) throws IOException {
    this( url, user, pass, null );
  }




  /**
   * Primary constructor.
   * 
   * @param url Complete URL to the physical instance
   * @param user ServiceNow user name to use 
   * @param pass Password credential for the user
   * @param prxy The HTTP Proxy workers should use to access the instance
   * 
   * @throws IOException if the URL is invalid
   */
  public Instance( final String url, final String user, final String pass, final Proxy prxy ) throws IOException {

    // If there is no proxy defined, check to see if the JRE has proxy settings 
    // defined by creating a new proxy and checking the value of proxy host set
    // by system properties. If there are proxy settings in the system 
    // properties, use them
    if ( prxy == null ) {
      // create a proxy object
      Proxy proxyCheck = new Proxy();
      
      // check to see if system properties have set the proxy host
      if ( StringUtil.isNotBlank( proxyCheck.getHost() ) ) {
        proxy = proxyCheck;
      } else {
        proxy = null;
      }
    } else {
      proxy = prxy;
    }

    try {
      baseurl = new URI( url.endsWith( "/" ) ? url : url + "/" );
    } catch ( final URISyntaxException e ) {
      throw new IOException( e );
    }

    username = user;
    password = pass;
    log.debug( "url=%s username=%s", baseurl, username );

    // Create the component responsible for interacting with the physical
    // ServiceNow instance based on the current state of the useReST flag
    if ( useReST ) {
      worker = new JsonWorker( this );
    } else {
      worker = new SoapWorker( this );
    }

    // define the core tables for the instance
    tables = getTable( ServiceNow.SYS_DB_OBJ );
    tables.setValidate( false );
    dictionary = getTable( ServiceNow.SYS_DICTIONARY );
    dictionary.setValidate( false );
    documentation = getTable( ServiceNow.SYS_DOCUMENTATION );
    documentation.setValidate( false );
  }




  /**
   * Close all resources (i,e, the worker) allocated to this instance.
   * 
   * @see java.io.Closeable#close()
   */
  @Override
  public void close() throws IOException {
    if ( worker != null ) {
      worker.close();
    }
  }




  /**
   * @return the defaultParameters
   */
  public Parameters getDefaultParameters() {
    return defaultParameters;
  }




  /**
   * Accessor to the table which contains all the information about the fields 
   * in the instance.
   * 
   * <p>This allows access to data in the {@code sys_dictionary} table in the 
   * instance.</p>
   * 
   * @return the dictionary table holding field information for each table
   */
  public SnowTable getDictionary() {
    return dictionary;
  }




  /**
   * Accessor to the table which contains help information about the fields in 
   * each table of the instance.
   * 
   * <p>This allows access to data in the {@code sys_documentation} table in the 
   * instance.</p>
   * 
   * @return the documentation/help/localization table
   */
  public SnowTable getDocumentation() {
    return documentation;
  }




  /**
   * @return return the hostname based on the currently set URL
   */
  public String getHost() {
    return baseurl.getHost();
  }




  /**
   * Returns a special SnowTable object which can be used for inserting and 
   * retrieving data from an import set.
   * 
   * <p><strong>NOTE:</strong> Import sets are special tables which allow 
   * special processing to occur in the instance. Different data is returned 
   * for an import set, so it can be important to know the type of table with
   * which you are working. In you need import set processing to occur, use 
   * import set tables.</p> 
   * 
   * @param name The name of the import set to represent
   * 
   * @return SnowTable An object with which to manipulate data
   * 
   * @throws IOException
   * @throws InvalidTableNameException
   */
  public SnowTable getImportSet( final String name ) throws IOException, InvalidTableNameException {
    SnowTable result;
    if ( tablemap.containsKey( name ) ) {
      result = tablemap.get( name );
    } else {
      result = new SnowImportSet( this, name );
      tablemap.put( name, result );
    }
    return result;
  }




  /**
   * @return the password the instance will use to authenticate the user
   */
  public String getPassword() {
    return password;
  }




  /**
   * @return return the port used to connect to the instance based on the currently set URL
   */
  public int getPort() {
    int retval = baseurl.getPort();
    if ( retval < 1 ) {
      if ( "HTTP".equalsIgnoreCase( baseurl.getScheme() ) ) {
        retval = 80;
      } else if ( "HTTPS".equalsIgnoreCase( baseurl.getScheme() ) ) {
        retval = 443;

      }
    }
    return retval;
  }




  /**
   * @return return the scheme used to connect to the instance based on the currently set URL
   */
  public String getScheme() {
    return baseurl.getScheme();
  }




  /**
   * Returns a SnowTable object which can be used for subsequent 
   * interactions with the instance.
   * 
   * @param tablename The name of the table to represent
   * 
   * @return SnowTable An object with which to manipulate data
   * 
   * @throws IOException
   * @throws InvalidTableNameException
   */
  public SnowTable getTable( final String tablename ) throws IOException, InvalidTableNameException {
    SnowTable result;
    if ( tablemap.containsKey( tablename ) ) {
      result = tablemap.get( tablename );
    } else {
      result = new SnowTable( this, tablename );
      tablemap.put( tablename, result );
    }
    return result;
  }




  /**
   * Accessor to the table which contains all the information about the tables 
   * in the instance.
   * 
   * <p>This allows access to data in the {@code sys_db_object} table in the 
   * instance.</p>
   * 
   * @return the table which contains all database table data in the instance.
   */
  public SnowTable getTables() {
    return tables;
  }




  /**
   * @return return the currently set URI
   */
  public URI getURI() {
    return baseurl;
  }




  /**
   * @return the name of the user account the instance will use to authenticate
   */
  public String getUser() {
    return username;
  }




  /**
   * @return the worker handling data exchange between the physical instance
   */
  public InstanceWorker getWorker() {
    return worker;
  }




  /**
   * Loads the schema data into the instance.
   * 
   * <p>The connecting user must have access to the {@code sys_dictionary} 
   * table in the instance for the load to succeed.</p>
   * 
   * <p>This is a very expensive operation as it queries 2 tables; 
   * sys_db_object and sys_dictionary. Loading of the schema data is not needed 
   * for simple interaction with the tables.</p>
   * 
   * @throws IOException if any communications errors have occurred.
   */
  public void loadSchema() throws IOException {

    tables.loadSchema();
    dictionary.loadSchema();
    documentation.loadSchema();

    // Since we have the schemas loaded we will load details for all the tables
    isUsingSchemas = true;
  }




  /**
   * Retrieve all the output messages from the ECC queue for the agent with the 
   * given name in a state of ready.
   * 
   * <p>This is a simple way to query for any pending work in the ECC queue for 
   * a particular agent.</p>
   * 
   * @param name The name of the agent
   * 
   * @return all the outbound messages for the named agent
   */
  public List<ECCMessage> getECCMessages( String name ) {
    List<ECCMessage> retval = new ArrayList<ECCMessage>();
    try {
      SnowTable table = getTable( ServiceNow.ECC_QUEUE );

      Parameters params = new Parameters( ServiceNow.ECC_QUEUE );
      params.setFilter( new SnowFilter( ECCMessage.AGENT, Predicate.IS, name ).and( ECCMessage.QUEUE, Predicate.IS, ECCMessage.OUTPUT_QUEUE ).and( ECCMessage.STATE, Predicate.IS, ECCMessage.READY_STATE ) );
      final List<SnowRecord> results = table.getRecords( params );
      for ( final SnowRecord record : results ) {
        ECCMessage msg = new ECCMessage( this, record );
        retval.add( msg );
      }

    } catch ( InvalidTableNameException | IOException e ) {
      log.debug( "Could not retrieve ECC Messages: %s - %s", e.getClass().getSimpleName(), e.getMessage() );
    }

    return retval;
  }




  /**
   * Attach the given attachment to the table and record specified in the 
   * attachment.
   * 
   * @param attachment
   */
  public void attach( SnowAttachment attachment ) {
    log.info( attachment.toString() );
    if ( attachment.getAttachmentId() == null ) {
      try {
        ECCMessage msg = new ECCMessage();
        msg.setAgent( "AttachmentCreator" );
        msg.setTopic( "AttachmentCreator" );
        msg.setName( attachment.getName() + ":" + attachment.getMimeType() );
        msg.setSource( attachment.getTableName() + ":" + attachment.getRecordId() );
        msg.setPayload( attachment.getEncodedContents() );

        // Attachments are always new requests to the ECC queue therefore "create"
        SnowRecord result = getTable( ServiceNow.ECC_QUEUE ).create( msg );
        if ( result != null ) {
          attachment.setAttachmentId( result.getKey() );
        }
      } catch ( InvalidTableNameException | IOException e ) {
        log.debug( "Could not attach file to record %s - %s", e.getClass().getSimpleName(), e.getMessage() );
      }
    } else {
      throw new IllegalArgumentException( "Attachment is already recorded in the system: sys_attachment:" + attachment.getAttachmentId() );
    }
  }




  /**
   * Add the given request decorator to the list of decorators.
   * 
   * <p>This allows for the modification of the HTTP request message just 
   * before it is sent. A common use case is the creation and population of 
   * specific headers in the request message such as a message identifier or 
   * key used by the web service infrastructure.</p>
   * 
   * @param decorator the component to add to the list of decorators used by 
   *        workers to this instance.
   */
  public void addRequestDecorator( RequestDecorator decorator ) {
    if ( decorator != null ) {
      requestDecorators.add( decorator );
    }
  }




  /**
   * @return the list of decorators to be used to enrich / transform the HTTP 
   *         requests before they are sent. 
   */
  public List<RequestDecorator> getRequestDecorators() {
    return requestDecorators;
  }




  /**
   * This allows for the invocation of scripted web services on an instance.
   * 
   * <p>It usefulness is currently being evaluated as there are many pre-
   * requisites for making the call and retrieving data. The web service must 
   * return data in a specific manner and SSO - SAML is causing issues in some 
   * environments.</p>
   * 
   * <p><strong>WARNING:</strong> This is currently being evaluated and not an 
   * official part of the API. Use with caution.</p>
   * 
   * @param endpoint The name of the in-bound web service.
   *  
   * @return a reference to a facade object allowing the invocation of web services.
   * 
   * @deprecated This is an experimental feature for backward compatibility
   */
  public SnowService getService( String endpoint ) {
    SnowService result;
    if ( servicemap.containsKey( endpoint ) ) {
      result = servicemap.get( endpoint );
    } else {
      result = new SnowService( this, endpoint );
      servicemap.put( endpoint, result );
    }
    return result;
  }




  /**
   * Instruct the instance to use the ReST interface (default) instead of SOAP 
   * (deprecated).
   */
  public static void useREST() {
    useReST = true;
  }




  /**
   * Instruct the instance to use SOAP (instead of ReST).
   */
  public static void useSOAP() {
    useReST = false;
  }




  /**
   * @return true indicates the instance is using ReST (default), false 
   *         indicates SOAP is being used.
   */
  public static boolean isREST() {
    return Instance.useReST;
  }




  /**
   * @return true if the HTTP messages should contain the authorization header 
   *         on all requests, false to wait until challenged (default).
   */
  public boolean isAuthenticatingPreemptively() {
    return authenticatePreemptively;
  }




  /**
   * Instructs the instance to send authentication with each request.
   * 
   * <p>This is false by default as it is generally considered less secure to 
   * send authorization data with every request. This instructs the instance to 
   * send the instance's username and password to every component (proxy and 
   * ServiceNow instance) regardless of the auth domain or if authorization was 
   * required.</p>
   * 
   * <p>This option is provided because some proxy / SOA gateway 
   * implementations will not send a challenge and just deny the connection. It 
   * is generally not recommended to set this to true unless it is required. It 
   * should not break anything, but one never knows. It is just another switch 
   * to flip when things are not working as expected.</p>
   * 
   * @param preemptive true means the client will send the Authorization header 
   *        without being challenged, false, will cause the client to wait for 
   *        authentication challenges before sending the authorization header.
   */
  public void setPreemptiveAuthentication( boolean preemptive ) {
    authenticatePreemptively = preemptive;
  }




  /**
   * @return the proxy settings this instance should use
   */
  public Proxy getProxySettings() {
    return proxy;
  }

}
