package coyote.snow;

/**
 * Exception thrown when the name of the table in the current operation does 
 * not match the name of any table in the current instance.
 */
public class InvalidTableNameException extends IllegalArgumentException {

  private static final long serialVersionUID = -3631698245576656092L;




  public InvalidTableNameException( final String name ) {
    super( "InvalidTableName: " + name );
  }

}