package coyote.snow;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class holds the the schema or definition for a ServiceNow table.
 * 
 * <p>The definition is pulled from the instance by the SnowTable 
 * constructor.</p>
 */
public class SnowTableSchema {

  private static final String ELEMENT_FIELD = "element";
  private static final String LANGUAGE_FIELD = "language";
  private static final String HELP_FIELD = "help";
  private static final String HINT_FIELD = "hint";
  private static final String NAME_FIELD = "name";
  private static final String ID_FIELD = "sys_id";

  private final SnowTable table;
  private final Instance instance;
  private String parentname = null;
  private final TreeMap<String, SnowFieldDefinition> fields;
  Logger log = LoggerFactory.getLogger( getClass() );

  // used for figuring out the schemas for database views 
  private static final String SYSDBVIEW = "sys_db_view";
  private static final String VIEW_TABLE = "sys_db_view_table";
  private static final String VIEW_TABLE_FIELD = "sys_db_view_table_field";
  private static final String SYS_ID = "sys_id";
  private static final String TABLE = "table";
  private static final String FIELD = "field";
  private static final String PREFIX = "variable_prefix";




  protected SnowTableSchema( final SnowTable table ) throws IOException, InvalidTableNameException {
    this.table = table;
    instance = table.getInstance();
    final String tablename = table.getName();

    log.debug( "getting table definition for %s", tablename );
    fields = new TreeMap<String, SnowFieldDefinition>();

    final SnowTable dictionary = instance.getDictionary();

    parentname = table.getParentName();
    log.debug( "%s parent is %s", tablename, parentname );

    if ( parentname != null ) {
      final SnowTableSchema parentDefinition = instance.getTable( parentname ).getSchema();
      final Iterator<SnowFieldDefinition> parentIter = parentDefinition.iterator();
      while ( parentIter.hasNext() ) {
        final SnowFieldDefinition parentField = parentIter.next();
        final String fieldname = parentField.getName();
        fields.put( fieldname, parentField );
      }
    }

    // get a list of of all the records in the dictionary table with a name
    // of the table for which this schema applies
    boolean empty = true;
    try {
      final SnowRecordList list = dictionary.getRecords( new Parameters( tablename, new SnowFilter( NAME_FIELD, Predicate.IS, tablename ) ) );

      final ListIterator<SnowRecord> iter = list.listIterator();
      while ( iter.hasNext() ) {
        final SnowRecord rec = iter.next();

        final String fieldname = rec.getFieldValue( ELEMENT_FIELD );
        if ( fieldname != null ) {

          final SnowFieldDefinition fieldDef = new SnowFieldDefinition( table, rec );
          fields.put( fieldname, fieldDef );
          log.trace( "%s.%s", tablename, fieldname );
          empty = false;

          if ( log.isTraceEnabled() && fieldDef.isReference() ) {
            final SnowFieldDefinition dvFieldDef = fieldDef.displayValueDefinition();
            fields.put( dvFieldDef.getName(), dvFieldDef );
            log.trace( "%s.%s", tablename, dvFieldDef.getName() );
          }
        }
      }

      try {
        final SnowRecordList documentation = instance.getDocumentation().getRecords( new Parameters( tablename, new SnowFilter( NAME_FIELD, Predicate.IS, tablename ).and( LANGUAGE_FIELD, Predicate.IS, "en" ) ) );
        final ListIterator<SnowRecord> dociter = documentation.listIterator();
        log.debug( "Retrieved a list of %s help records", documentation.size() );
        while ( dociter.hasNext() ) {
          final SnowRecord rec = dociter.next();

          final String fieldname = rec.getFieldValue( ELEMENT_FIELD );
          final String helptxt = rec.getFieldValue( HELP_FIELD );
          final String hinttxt = rec.getFieldValue( HINT_FIELD );

          // get the field definition for the this documentation record
          final SnowFieldDefinition fieldDef = getFieldDefinition( fieldname );

          // if there is a field definition with this element name...
          if ( fieldDef != null ) {

            // Set the help text if there is any
            if ( helptxt != null ) {
              fieldDef.setHelp( helptxt );
            }

            // Set the hint text if there is any
            if ( hinttxt != null ) {
              fieldDef.setHint( hinttxt );
            }

          } // fielddef !null
        } // doc iterator

      } catch ( final Exception e ) {
        log.warn( "Problems loading documentation for schema - %s : %s", e.getClass().getSimpleName(), e.getMessage() );
      }

    } catch ( Exception e ) {
      // Apparently the getRecords on the dictionary failed...probably a view
      log.debug( "Could not get dictionary records for %s reason: '%s', trying as a database view", tablename, e.getMessage() );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // If there are no table definitions, try looking it up as a view

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    if ( empty ) {

      // Get the instance table for the sys_db_view
      SnowTable tableview = instance.getTable( SYSDBVIEW );

      // get a list of 
      List<SnowRecord> tableResults = tableview.getRecords( new SnowFilter( "name", Predicate.IS, tablename ) );

      if ( tableResults.size() > 0 ) {
        log.info( "Retrieving schema for database view, this may take a little while to build view composition..." );

        SnowRecord viewEntry = tableResults.get( 0 );

        tableview = instance.getTable( VIEW_TABLE );
        tableResults = tableview.getRecords( new SnowFilter( "view", Predicate.IS, viewEntry.getAsString( SYS_ID ) ) );

        if ( tableResults.size() > 0 ) {
          log.debug( "Found the following (%s) tables composing '%s' view", tableResults.size(), tablename );

          // For each of the composing tables
          for ( SnowRecord tableRecord : tableResults ) {
            String tableName = tableRecord.getAsString( TABLE );
            String prefix = tableRecord.getAsString( PREFIX );
            String vtableid = tableRecord.getAsString( SYS_ID );
            log.debug( "Composing Table: %s - prefix: %s (%s)", tableName, prefix, vtableid );

            // Do a query for all the fields in the view table
            List<SnowRecord> ftableResults = instance.getTable( VIEW_TABLE_FIELD ).getRecords( new SnowFilter( "view_table", Predicate.IS, vtableid ) );

            // for each field in the view table
            for ( SnowRecord ftableRecord : ftableResults ) {

              final SnowFieldDefinition fieldDef = instance.getTable( ftableRecord.getAsString( TABLE ) ).getSchema().getFieldDefinition( ftableRecord.getAsString( FIELD ) );
              if ( fieldDef != null ) {
                // view fields contain a prefix
                String fldname = prefix + "_" + fieldDef.getName();
                fields.put( fldname, fieldDef );
                log.debug( "Composing table field #%s - %s.%s as '%s' %s(%s) - %s", fields.size(), tablename, fieldDef.getName(), fldname, fieldDef.getType(), fieldDef.getLength(), fieldDef.getLabel() );
                empty = false;
              } else {
                log.warn( "Could not determine the field definition for %s.%s -> Table Record:", tablename, ftableRecord.getAsString( FIELD ), ftableRecord );
              }
            }
          }
        } else {
          log.debug( "View '%s' did not have any composing tables", tablename );
        }

      } else {
        // Apparently this is not a view either
        log.debug( "View '%s' not found", tablename );
      }

    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    if ( empty ) {
      log.error( "Unable to read schema for '%s' - no elements", tablename );
      if ( tablename.equals( ServiceNow.SYS_DB_OBJ ) || tablename.equals( ServiceNow.SYS_DICTIONARY ) ) {
        throw new InsufficientRightsException( table, "getRecords", "Unable to read " + tablename, null );
      } else {
        throw new InvalidTableNameException( tablename );
      }
    }
  }




  /**
   * Format the schema into a string useful for debugging.
   * 
   * @return String representation of the schema.
   */
  public String dump() {
    final StringBuffer b = new StringBuffer();
    final String tablename = table.getName();
    b.append( "Table Definition for " );
    b.append( tablename );
    if ( parentname != null ) {
      b.append( " - child of '" );
      b.append( parentname );
      b.append( "'" );
    }
    b.append( "\r\n" );
    int row = 0;
    final Iterator<SnowFieldDefinition> iter = iterator();
    while ( iter.hasNext() ) {
      final SnowFieldDefinition fd = iter.next();
      final String fieldname = fd.getName();
      final String fieldtype = fd.getType();
      final int fieldlen = fd.getLength();
      final String fieldlabel = fd.getLabel();
      String fieldref = fd.getReference();
      String fieldhint = fd.getHint();

      if ( fieldref == null )
        fieldref = "";
      else
        fieldref = "[" + fieldref + "] ";

      // provide a little separation between hint text if it exists
      if ( fieldhint.length() > 0 ) {
        fieldhint = "- " + fieldhint;
      }

      fieldhint = fieldref + fieldhint;

      b.append( String.format( "%d %s %s(%d) '%s' %s\r\n", ++row, fieldname, fieldtype.toUpperCase(), fieldlen, fieldlabel, fieldhint ) );
    }
    return b.toString();
  }




  /**
   * Return the field definition for the field with the given name.
   * 
   * @param fieldname the name of the field to return (Case Sensitive)
   * 
   * @return the field definition for the field with the given name or null if no fild with that name exists in the schema
   */
  public SnowFieldDefinition getFieldDefinition( final String fieldname ) {
    if ( fieldname != null ) {
      return fields.get( fieldname );
    } else {
      return null;
    }
  }




  /**
   * @return the field definitions contained in this schema as a collection
   */
  public Collection<SnowFieldDefinition> getFieldDefinitions() {
    return fields.values();
  }




  public String[] getFieldNames() {
    return fields.keySet().toArray( new String[0] );
  }




  public SnowFieldDefinition getKeyFieldDefinition() {
    return fields.get( ID_FIELD );
  }




  public boolean isDefined( final String fieldname ) {
    if ( fieldname != null ) {
      return ( fields.get( fieldname ) != null );
    } else {
      return false;
    }
  }




  public Iterator<SnowFieldDefinition> iterator() {
    return getFieldDefinitions().iterator();
  }




  public int numFields() {
    return fields.size();
  }




  /**
   * @return the table to which this schema belongs
   */
  public SnowTable getTable() {
    return table;
  }

}
