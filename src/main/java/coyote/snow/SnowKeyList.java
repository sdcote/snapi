package coyote.snow;

import java.util.ArrayList;


/**
 * This class holds a list of GUIDs (or Sys_Ids) as returned from a getKeys call. 
 */
public class SnowKeyList extends ArrayList<SnowKey> {

  private static final long serialVersionUID = 1L;




  public SnowKeyList() {
    super();
  }




  public SnowKeyList( final int size ) {
    super( size );
  }




  public SnowFilter filter() {
    return new SnowFilter( "sys_id", Predicate.IN, toString() );
  }




  public SnowFilter filter( final int fromIndex, final int toIndex ) {
    return new SnowFilter( "sys_id", Predicate.IN, getSlice( fromIndex, toIndex ) );
  }




  /**
   * Returns a subset of the list as comma separated string.
   * 
   * <p>Used to construct encoded queries. The number of entries returned is 
   * (toIndex - fromIndex). An exception may occur if toIndex < 0 or 
   * fromIndex > size()</p> 
   * 
   * @param fromIndex Zero based starting index (inclusive).
   * @param toIndex Zero based ending index (exclusive).
   * 
   * @return A comma separated list of SysIds suitable for use in 
   * constructing an encoded query.
   */
  private String getSlice( final int fromIndex, final int toIndex ) {
    final StringBuilder result = new StringBuilder();
    boolean first = false;
    for ( int i = fromIndex; i < toIndex; ++i ) {
      if ( first ) {
        first = false;
      } else {
        result.append( "," );
      }
      result.append( get( i ).toString() );
    }
    return result.toString();
  }




  @Override
  public int size() {
    return super.size();
  }




  @Override
  public String toString() {
    return getSlice( 0, size() );
  }
}
