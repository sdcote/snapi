package coyote.snow;

import java.util.Iterator;
import java.util.ListIterator;


/**
 * An iterator used to loop through a SnowRecordList.
 */
public class SnowRecordIterator implements Iterator<SnowRecord> {

  private final ListIterator<SnowRecord> iter;




  SnowRecordIterator( final SnowRecordList list ) {
    iter = list.listIterator();
  }




  @Override
  public boolean hasNext() {
    return iter.hasNext();
  }




  @Override
  public SnowRecord next() {
    return iter.next();
  }




  @Override
  public void remove() {
    iter.remove();
  }

}
