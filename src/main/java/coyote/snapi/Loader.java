/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snapi;

import coyote.commons.Version;


/**
 * This is a utility launcher for the library.
 */
public class Loader {

  private static final String NAME = "SNapi";

  private static final Version VERSION = new Version( 0, 2, 0, Version.DEVELOPMENT );




  /**
   * @param args
   */
  public static void main( final String[] args ) {

      System.out.println( NAME + " v" + VERSION );
      System.exit( 0 );
  } // main

}
