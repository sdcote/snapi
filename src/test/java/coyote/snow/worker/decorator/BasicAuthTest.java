/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow.worker.decorator;

//import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * 
 */
public class BasicAuthTest {

  @Test
  public void test() {

    // Test with the data from RFC2617
    BasicAuth subject = new BasicAuth();
    subject.setUsername( "Aladdin" );
    subject.setPassword( "open sesame" );
    subject.calculateHeaderData();
    assertEquals( subject.getHeaderData(), "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==" );
  }

}
