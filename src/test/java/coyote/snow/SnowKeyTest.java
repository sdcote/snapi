/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

//import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;


/**
 * 
 */
public class SnowKeyTest {

  /**
   * Test method for {@link coyote.snow.SnowKey#SnowKey(java.lang.String)}.
   */
  @Test
  public void testSnowKey() {
    SnowKey key = new SnowKey( "12345678901234567890123456789012" );
    assertNotNull( key );
    key = new SnowKey( "  12345678901234567890123456789012 " );
    assertNotNull( key );

    key = new SnowKey( "6e48a6af0fdbb100a5eee478b1050e1c" );
    assertNotNull( key );

    key = new SnowKey( "\b6e48a6af0fdbb100a5eee478b1050e1c\n\t\r" );
    assertNotNull( key );

    try {
      key = new SnowKey( null );
      fail( "Null keys are not allowed" );
    } catch ( Exception e ) {}
    try {
      key = new SnowKey( "" );
      fail( "Empty keys are not allowed" );
    } catch ( Exception e ) {}

  }




  /**
   * Test method for {@link coyote.snow.SnowKey#toString()}.
   */
  @Test
  public void testToString() {
    SnowKey key = new SnowKey( "  12345678901234567890123456789012 " );
    assertEquals( "12345678901234567890123456789012", key.toString() );

  }

}
