/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

//import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * 
 */
public class SnowFilterTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}




  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}




  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}




  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}




  /**
   * Test method for {@link coyote.snow.SnowFilter#SnowFilter()}.
   */
  @Test
  public void testSnowFilter() {
    SnowFilter filter = new SnowFilter();
    assertNotNull( filter );
  }




  /**
   * 
   */
  @Test
  public void testAndFilter() {
    SnowFilter filter = new SnowFilter();
    assertNotNull( filter );
    assertTrue(filter.isEmpty());
    filter.and( "caller_id", Predicate.IS, "12345" )
    .and( "active", Predicate.IS, "true" )
    .or( "name", Predicate.IS, "printer" );
    
    assertFalse(filter.isEmpty());

    System.out.println( filter.toString() );
    System.out.println( filter.toEncodedString() );

  }

}
