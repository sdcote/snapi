/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.snow;

//import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


/**
 * 
 */
public class ParametersTest {

  /**
   * Test method for {@link coyote.snow.Parameters#getDisplayValues()}.
   */
  @Test
  public void testGetDisplayValues() {
    Parameters params = new Parameters();
    assertFalse( params.isDisplayingValues() );
  }




  /**
   * Test method for {@link coyote.snow.Parameters#setDisplayValues(boolean)}.
   */
  @Test
  public void testSetDisplayValues() {
    Parameters params = new Parameters();
    assertFalse( params.isDisplayingValues() );
    params.setDisplayValues( true );
    assertTrue( params.isDisplayingValues() );
    params.setDisplayValues( false );
    assertFalse( params.isDisplayingValues() );
  }




  /**
   * Test method for {@link coyote.snow.Parameters#setDisplayValuesAll()}.
   */
  @Test
  public void testSetDisplayValuesAll() {
    Parameters params = new Parameters();
    assertFalse( params.isDisplayingValues() );
    params.setDisplayValuesAll();
    assertTrue( params.isDisplayingValues() );
  }




  @Test
  public void testFieldLists() {
    Parameters params = new Parameters();
    assertTrue( params.getFieldListSize() == 0 );
    assertNotNull( params.getFieldList() );

    params.addField( "sys_id" );
    assertTrue( params.getFieldListSize() == 1 );

    params.addField( "name" );
    assertTrue( params.getFieldListSize() == 2 );

    String fieldlist = params.getFieldList();
    assertEquals( "sys_id,name", fieldlist );
  }

}
