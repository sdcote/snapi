/*
 * Copyright (c) 2015 Stephan D. Cote' - All rights reserved.
 * 
 * This program and the accompanying materials are made available under the 
 * terms of the MIT License which accompanies this distribution, and is 
 * available at http://creativecommons.org/licenses/MIT/
 *
 * Contributors:
 *   Stephan D. Cote 
 *      - Initial concept and initial implementation
 */
package coyote.itsm;

//import static org.junit.Assert.*;
//import org.junit.Test;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import coyote.snow.Parameters;
import coyote.snow.Predicate;
import coyote.snow.SnowFilter;
import coyote.snow.SnowRecord;


/**
 * 
 */
public class ServiceManagerTest {
  private static final Logger LOG = LoggerFactory.getLogger( ServiceManagerTest.class );




  /**
   * 
   */
  public void test() {

    // Get instance connection data from the system properties loaded above
    final String instanceUrl = System.getProperty( "instance.url" );
    final String instanceUser = System.getProperty( "instance.user" );
    final String instancePass = System.getProperty( "instance.pass" );

    try (ServiceManager manager = new ServiceManager( instanceUrl, instanceUser, instancePass )) {

      // Create a service request
      final Request request = manager.buildRequest();

      // Set some values
      request.setShortDescription( "Need access to department file shares" );

      // now create the request
      request.create();

      LOG.info( request.toString() );

    } catch ( final Exception e ) {
      LOG.error( e.getMessage() );
    }
  }




  /**
   * 
   */
  public void testQuery() {

    // Get instance connection data from the system properties loaded above
    final String instanceUrl = System.getProperty( "instance.url" );
    final String instanceUser = System.getProperty( "instance.user" );
    final String instancePass = System.getProperty( "instance.pass" );

    try (ServiceManager manager = new ServiceManager( instanceUrl, instanceUser, instancePass )) {

      // create a query like "state=1^caller_idLIKEnetcool"
      final SnowFilter filter = new SnowFilter();

      // filter clauses can be chained together with ANDs and ORs
      filter.and( "state", Predicate.IS, "1" ).and( "caller_id", Predicate.CONTAINS, "netcool" );

      // Create a new request parameters object for the incident table and our
      // filter we created above/
      final Parameters params = new Parameters( "incident", filter );
      params.setLimit( 20 ); // don't return more than 20 records
      params.setDisplayValues( true ); // needed to see comments and work notes

      // Execute the request
      final List<SnowRecord> results = manager.getRecords( params );

      // Print out our results
      for ( final SnowRecord record : results ) {
        LOG.info( record.toFormattedString() );
      };

    } catch ( final Exception e ) {
      LOG.error( e.getMessage() );
    }
  }




  public void testDelete() {

    // Get instance connection data from the system properties loaded above
    final String instanceUrl = System.getProperty( "instance.url" );
    final String instanceUser = System.getProperty( "instance.user" );
    final String instancePass = System.getProperty( "instance.pass" );

    try (ServiceManager manager = new ServiceManager( instanceUrl, instanceUser, instancePass )) {

      // Create a service request
      final Incident incident = manager.buildIncident( "b020bf0a0fab3900085d6509b1050ef2" );

      // Execute the request to delete the record identified in the parameters
      if ( incident.delete() ) {
        LOG.info( "Successfully deleted {} from {} table.", incident.getIdentifier(), incident.getTablename() );
      } else {
        LOG.error( "Could Not Delete {} from {} table - Not Found.", incident.getIdentifier(), incident.getTablename() );
      }

    } catch ( final Exception e ) {
      LOG.error( e.getMessage() );
    }
  }

}
